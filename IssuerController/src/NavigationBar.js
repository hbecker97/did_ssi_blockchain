import React, { useContext, useState, useEffect } from "react";
import { useSession, LoginButton, LogoutButton } from "@inrupt/solid-ui-react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";

import "./NavigationBar.css";

const NavigationBar = () => {
  const { session } = useSession();
  const [idp, setIdp] = useState("https://inrupt.net");
  const [currentUrl, setCurrentUrl] = useState("https://localhost:3002");

  useEffect(() => {
    setCurrentUrl(window.location.href);
  }, [setCurrentUrl]);

  return (
    <>
      <Navbar bg="dark" variant="dark" expand="lg">
        <Navbar.Brand as={Link} to="/">
          Issuer Controller
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/demo">
              Demo
            </Nav.Link>
          </Nav>
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/schema/create">
              Create credential schema
            </Nav.Link>
          </Nav>
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/connections">
              Manage connections
            </Nav.Link>
          </Nav>
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/credentials">
              Manage credential requests
            </Nav.Link>
          </Nav>
          <Nav className="ms-auto">
            {!session.info.isLoggedIn ? (
              <LoginButton
                oidcIssuer={idp}
                redirectUrl={currentUrl}
                onError={console.error}
              />
            ) : (
              <div className="white-text">
                Logged In as:{" "}
                <span className="login-information">
                  {session.info.webId.split("/")[2].split(".")[0]}
                </span>
                <LogoutButton />
              </div>
            )}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default NavigationBar;
