export const createInvitation = async (url) => {
  const data = {}
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data)
  });
  return response.json()
};

export const getConnections = async (url) => {

  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const cons = json.results

  const result = new Array()
  for (let index = 0; index < cons.length; index++) {

    let object = cons[index]
    let con = null
    if(object.hasOwnProperty("their_label")){
      let date = new Date(object.created_at)
      let formatted_date = date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes()

      con = new Connection(object.connection_id, formatted_date, object.alias, object.state, object.their_label, object.their_did, object.my_did, object.their_public_did);
    }else{
      let date = new Date(object.created_at)
      let formatted_date = date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes()

      con = new Connection(object.connection_id, formatted_date, object.alias, object.state, null);
    }

    result.push(con)
  }

  return result
};

export const deleteConnection = async (url) => {

  const response = await fetch(url, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;
};

export const createSchema = async (url, name, attributes, url2) => {

  // create schema first
  const data = {
      "attributes": attributes,
      "schema_name": name,
      "schema_version": "1.0"
    };

  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json"
    },
    body: JSON.stringify(data)
  });

  // second is the credential definition creation based on the created schemas
  if (response.ok) {
      const json = await response.json();
      const schema_id = json.schema_id

      const data2 = {
        "revocation_registry_size": 1000,
        "schema_id": schema_id,
        "support_revocation": false,
        "tag": "default"
      }

      const response2 = await fetch(url2, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: JSON.stringify(data2)
      });

      if(response2.ok) {
          console.log('credential definition created.')
      }
    }
};

export const getSchema = async (url, url2) => {
  // first get call to get schema ids
  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const schema_ids = json.schema_ids


  // second get call to get schema_ids
  const result = new Array()
  for (let index = 0; index < schema_ids.length; index++) {
    const url_con =  url2 + schema_ids[index]

    const response = await fetch(url_con, {
      headers: {
        Accept: "application/json",
      },
    });

    if (response.status !== 200) return null;

    // parse json object
    const json = await response.json();

    let schema = new Schema(json.schema.name, json.schema.attrNames);
    result.push(schema)
  }

  return result

};

export const getCredentialDefinitions = async (url, url2) => {

  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  let definitions = await response.json()
  definitions = definitions.credential_definition_ids

  // get every credential definition object
  const result = new Array()
  for (let index = 0; index < definitions.length; index++) {

    const response = await fetch(url2 + definitions[index], {
      headers: {
        Accept: "application/json",
      },
    });

    // parse json object
    let json = await response.json();
    json = json.credential_definition

    // parse fields of credential definition
    let attributes = []
    let fields = json.value.primary.r
    delete fields["master_secret"];

    for(var k in fields) attributes.push(k);

    const cred = new CredentialDef(json.id, attributes);

    result.push(cred)
  }

  return result;
};

export const getCredentialRequests = async (url) => {

  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  let requests = await response.json()
  requests = requests.results

  // save all elements that are requests in list
  const result = new Array()

  for (let index = 0; index < requests.length; index++) {

    // check if credential request
    if(requests[index].cred_ex_record.by_format.hasOwnProperty('cred_proposal')){

        // save JSON object in CredentialRequest object
        const name = await getNameByConnectionID(requests[index].cred_ex_record.connection_id)
        const req = new CredentialRequest(requests[index].cred_ex_record.cred_ex_id,
                                          requests[index].cred_ex_record.connection_id,
                                          name,
                                          requests[index].cred_ex_record.by_format.cred_proposal.indy.schema_name,
                                          requests[index].cred_ex_record.cred_preview.attributes
                                        );
        result.push(req)
    }
  }

  return result;
};

async function parseAttributes(attributes){
  const att_list = attributes.split(',');
  let result = []
  for (let index = 0; index < att_list.length; index++) {
      const lst = att_list[index].split(':');

      let str = ""
      if(lst.length > 2){
       str = { "name": lst[0] , "value": lst[1] + ":" + lst[2] }
      }else{
       str = { "name": lst[0] , "value": lst[1] }
      }

      result.push(str)
  }

  return result
}

async function getPublicDID(){
  const response = await fetch("http://localhost:8031/wallet/did/public", {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  let _did = await response.json()

  return _did.result.did
}

export const sendCredentials = async (url, conID, schemaName, attributes, credRecord) => {
  // parese attributes
  let att_list = await parseAttributes(attributes)

  // retrieve own public DID
  const did = await getPublicDID()

  // create schema_id by schema name
  let schema_id = did + ":2:" + schemaName + ":1.0"

  // get schema object by schema_ids
  let response = await fetch("http://localhost:8031/schemas/" + schema_id, {
    headers: {
      Accept: "application/json",
    },
  });

  response = await response.json()

  // create credential definition id
  let cred_df_id = response.schema.seqNo
  let cred_df = did + ":3:CL:" + cred_df_id + ":default"

  const data = {
    "auto_remove": true,
    "comment": "",
    "connection_id": conID,
    "credential_preview": {
      "@type": "issue-credential/2.0/credential-preview",
      "attributes": att_list
    },
    "filter": {
      "indy": {
        "cred_def_id": cred_df,
        "issuer_did": did,
        "schema_id": schema_id,
        "schema_issuer_did": did,
        "schema_name": schemaName,
        "schema_version": "1.0"
      }
    },
    "trace": true
  };

  const response2 = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data)
  });

  // delete credential request records
  const response3 = await fetch("http://localhost:8031/issue-credential-2.0/records/" + credRecord, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
    },
  });
};

export const deleteCredentialRequest = async (credRecord) => {
  // delete credential request records
  const response = await fetch("http://localhost:8031/issue-credential-2.0/records/" + credRecord, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
    },
  });
}

export const getNameByConnectionID = async (conID) => {

  const response = await fetch("http://localhost:8031/connections", {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const reqs = json.results

  let result = ""
  for (let index = 0; index < reqs.length; index++) {
    if(reqs[index].connection_id == conID){
      result = reqs[index].their_label
    }
  }

  return result
};

class Schema {
  constructor(name, attList, schema_id, cred_id) {
    this.name = name;
    this.attributes = attList;
  }
}

class Connection {
  constructor(connection_id, created_at, alias, state, theirLabel, theirDID, myDID, theirpubplicDID) {
    this.connection_id = connection_id;
    this.created_at = created_at;
    this.alias = alias;
    this.state = state;
    this.theirLabel = theirLabel
    this.theirDID = theirDID;
    this.myDID = myDID;
    this.theirpubplicDID = theirpubplicDID;
  }
}

  class CredentialDef {
    constructor(cred_id, fields) {
      this.cred_id = cred_id;
      this.fields = fields;
    }
  }

  class CredentialRequest {
    constructor(cred_ex_id, connection_id, name, schema_name,attributes) {
      this.cred_ex_id = cred_ex_id;
      this.connection_id = connection_id;
      this.name = name;
      this.schema_name = schema_name;
      this.attributes = attributes;
    }
  }
