import React, { useContext, useState, useEffect } from "react";
import { Form, Button, Col, Card, ListGroup, Alert, Table } from "react-bootstrap";

import { getCredentialRequests, sendCredentials } from "../lib/agentCom";

const ManageCredentials = () => {
  const [requests, changeRequests] = useState([]);
  const [show, setShow] = useState(false);
  const [conID, changeConID] = useState("");
  const [attList, changeAttList] = useState("");
  const [schema, changeSchema] = useState("");
  const [credRecord, changeCredRecord] = useState("");

  useEffect(async ()=> {
    const response = await getCredentialRequests("http://localhost:8031/issue-credential-2.0/records")
    changeRequests(response)
   }, [] );


   const renderRequests = () => {
       return (
         <div>
           <Table className="mx-6 my-3" striped bordered hover size="sm">
             <thead>
               <tr>
                 <th>Requester name</th>
                 <th>Connection ID</th>
                 <th>Credential record</th>
                 <th>Schema name</th>
                 <th>Requested attributes</th>
               </tr>
             </thead>
             <tbody>
               {requests.map((item, i) => {
                 return (
                   <tr key={i}>
                     <td>{item.name}</td>
                     <td>{item.connection_id}</td>
                     <td>{item.cred_ex_id}</td>
                     <td>{item.schema_name}</td>
                     <td><ul>
                       {
                         item.attributes.map((item2, i2) => {
                           return (
                             <li key={i2}>Name: '{item2.name}', Value: '{item2.value}'</li>
                         )})
                       }
                     </ul></td>
                   </tr>
                 );
               })
               }
             </tbody>
           </Table>
         </div>
       );
     };

     function sleep(ms) {
         return new Promise(resolve => setTimeout(resolve, ms));
      }

     async function btnsendCred() {
       await sendCredentials("http://localhost:8031/issue-credential-2.0/send", conID, schema, attList, credRecord)
       setShow(true)
       await sleep(2000);
       window.location.reload(false);
     }

  return (
    <>
    <h4 className="mx-5 my-3">Received credential requests</h4>

    { renderRequests()}

    <h4 className="mx-5 my-3">Send credentials</h4>

      <Form className="mx-5 my-3">
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Connection ID: </Form.Label>
              <Form.Control
                type="text"
                autoComplete="off"
                placeholder="Enter connection ID..."
                value={conID}
                onChange={(e) => changeConID(e.target.value)}
              />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Credential record: </Form.Label>
              <Form.Control
                type="text"
                autoComplete="off"
                placeholder="Enter credential record..."
                value={credRecord}
                onChange={(e) => changeCredRecord(e.target.value)}
              />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Attributes: </Form.Label>
              <Form.Control
                type="text"
                autoComplete="off"
                placeholder="Name1:Value1,Name2:Value2..."
                value={attList}
                onChange={(e) => changeAttList(e.target.value)}
              />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Schame name: </Form.Label>
              <Form.Control
                type="text"
                autoComplete="off"
                placeholder="Enter schema name..."
                value={schema}
                onChange={(e) => changeSchema(e.target.value)}
              />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-md-center">
          <Form.Group>
            <Button variant="outline-primary" onClick={btnsendCred}>Send</Button>
          </Form.Group>
        </Form.Row>
      </Form>

      <Alert show={show} variant="success" className="mx-5 my-3">
          <Alert.Heading>Credentials have been sent.</Alert.Heading>
          <div className="d-flex justify-content-end">
            <Button onClick={() => setShow(false)} variant="outline-success">
              Close
            </Button>
          </div>
      </Alert>

    </>
  );
};

export default ManageCredentials;
