import React, { useContext, useState, useEffect } from "react";
import { Form, Button, Col, Card, ListGroup, Alert } from "react-bootstrap";

import { createInvitation, getConnections, deleteConnection } from "../lib/agentCom";

const ManageConnections = () => {
  const [invitation, changeInvitation] = useState("");
  const [show, setShow] = useState(false);
  const [connections, changeConnections] = useState([]);
  const [alias, changeAlias] = useState("");

  useEffect(async ()=> {
    const response = await getConnections("http://localhost:8031/connections")
    changeConnections(response)
    //console.log(response)
   }, [] );

  async function btncreateInvitation(e) {
    const response = await createInvitation("http://localhost:8031/connections/create-invitation?alias=" + alias)

    let inv = ""
    inv = response.invitation
    inv = JSON.stringify(inv)

    changeInvitation(inv)
    setShow(true)
  }

  async function btndeleteConnection(id) {

    await deleteConnection("http://localhost:8031/connections/" + id)
    window.location.reload(false);
  }

  const renderConnections = () => {
      return (
        <div>
        {connections.map((item, i) => {
          return (
            <Card className="mx-5 my-3 text-center"  key={i}>
              <Card.Header as="h5">Connection ID: {item.connection_id}</Card.Header>
              <Card.Body>
                {item.state === "active" && (
                  <Card.Text>
                    <b>Created at: </b> {item.created_at} <br></br>
                    <b>Alias: </b> {item.alias} <br></br>
                    <b>State: </b> {item.state} <br></br>
                    <b>Connected with: </b> {item.theirLabel}, <b>their DID: </b>{item.theirDID} <br></br>
                  </Card.Text>
                )}
                {item.state !== "active" && (
                  <Card.Text>
                    <b>Created at: </b> {item.created_at} <br></br>
                    <b>Alias: </b> {item.alias} <br></br>
                    <b>State: </b> {item.state} <br></br>
                  </Card.Text>
                )}
                <Button variant="danger" onClick={() => btndeleteConnection(item.connection_id)}>Delete connection</Button>
              </Card.Body>
            </Card>
          );
        })}
        </div>
      );
    };

  return (
    <>
    <h2 className="mx-5 my-3"> Create invitation</h2>
      <Form className="mx-5 my-3">
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Connection alias: </Form.Label>
              <Form.Control
                type="text"
                autoComplete="off"
                placeholder="Enter connection alias..."
                value={alias}
                onChange={(e) => changeAlias(e.target.value)}
              />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-md-center">
          <Form.Group>
            <Button variant="outline-primary" onClick={btncreateInvitation}>New invitation</Button>
          </Form.Group>
        </Form.Row>
      </Form>

    <Alert show={show} variant="success" className="mx-5 my-3">
        <Alert.Heading>Please hand the invitation to the other party:</Alert.Heading>
        <p>
          {invitation}
        </p>
        <hr />
        <div className="d-flex justify-content-end">
          <Button onClick={() => setShow(false)} variant="outline-success">
            Close
          </Button>
        </div>
    </Alert>

    <h2 className="mx-5 my-3"> Manage connections</h2>
    { renderConnections()}
    </>
  );
};

export default ManageConnections;
