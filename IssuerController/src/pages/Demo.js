import React, { useContext, useState, useEffect } from "react";
import { Container, Form, Button, Col, Card, ListGroup, Alert, Accordion, Table } from "react-bootstrap";

import { createSchema, deleteCredentialRequest, getCredentialRequests, sendCredentials, getSchema, createInvitation, getConnections, deleteConnection } from "../lib/agentCom";
import { Chrono } from "react-chrono";

import { getProfile, alterQuad } from "../lib/solidCom";

import { useSession } from "@inrupt/solid-ui-react";

import solid from '../Images/solid.jpeg';
import did from '../Images/did.jpeg';
import ethereum from '../Images/ethereum.png';

const Demo = () => {
  const { session } = useSession();

  // profile & connections
  const [profile, changeProfile] = useState([]);
  const [invitation, changeInvitation] = useState("");
  const [show, setShow] = useState(false);
  const [connections, changeConnections] = useState([]);
  const [alias, changeAlias] = useState("");

  // create schemas
  const [name, changeName] = useState("");
  const [attributes, changeAttributes] = useState("");
  const [schemas, changeSchemas] = useState([]);
  const [show2, setShow2] = useState(false);

  // credential management
  const [requests, changeRequests] = useState([]);
  const [show3, setShow3] = useState(false);
  const [show4, setShow4] = useState(false);
  const [conID, changeConID] = useState("");
  const [attList, changeAttList] = useState("");
  const [schema, changeSchema] = useState("");
  const [credRecord, changeCredRecord] = useState("");

  const items = [{
      title: "1",
      cardTitle: "Create credential schema",
      //cardSubtitle:"",
      cardDetailedText: "Create the schema for credentials on the distributed ledger.",
    },
    {
        title: "2",
        cardTitle: "Create connection offers",
        //cardSubtitle:"",
        cardDetailedText: "Hand the connection offers to Holder to establish a connection.",
    },
    {
        title: "3",
        cardTitle: "Issue credentials",
        //cardSubtitle:"",
        cardDetailedText: "Issue credentials of the created schma with certain values.",
    }
  ];

  useEffect(async ()=> {
    const response = await getConnections("http://localhost:8031/connections")
    changeConnections(response)

    const card = "https://acaissuer.inrupt.net/profile/card#me";
    let pro = await getProfile(card, session.fetch)
    changeProfile(pro)

    const respond = await getSchema("http://localhost:8031/schemas/created", "http://localhost:8031/schemas/")
    changeSchemas(respond)

    const response2 = await getCredentialRequests("http://localhost:8031/issue-credential-2.0/records")
    changeRequests(response2)
   }, [] );

  async function btndeleteConnection(id) {

    await deleteConnection("http://localhost:8031/connections/" + id)
    window.location.reload(false);
  }

  function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
   }

  async function btnconnect() {

    setShow(true)
    //await createConnection("http://localhost:8031/connections/receive-invitation", invitation)
    await sleep(2000);
    window.location.reload(false);
  }

  async function btncreateInvitation(e) {
    const response = await createInvitation("http://localhost:8031/connections/create-invitation?alias=" + alias)

    let inv = ""
    inv = response.invitation
    inv = JSON.stringify(inv)

    changeInvitation(inv)
    setShow(true)
  }

  async function submitForm(e) {
    e.preventDefault();
    const att_list = attributes.split(',');
    let list = "["
    for (let index = 0; index < att_list.length; index++) {
        list += '"' + att_list[index] + '",'
    }
    list = list.substring(0, list.length - 1);
    list += "]"

    await createSchema("http://localhost:8031/schemas", name, att_list, "http://localhost:8031/credential-definitions");

    setShow2(true)

  }

  async function btnsendCred(item) {
    // parse attributes into String
    let atts = ""
    for (let key in item.attributes) {
      let name = item.attributes[key].name;
      let value = item.attributes[key].value;
      atts += name + ":" + value + ","
    }
    atts = atts.slice(0, -1)

    await sendCredentials("http://localhost:8031/issue-credential-2.0/send", item.connection_id, item.schema_name, atts, item.cred_ex_id)
    setShow3(true)
    await sleep(3000);
    window.location.reload(false);
  }

  async function btndeleteRequest(item) {
    await deleteCredentialRequest(item.cred_ex_id)
    setShow4(true)
    await sleep(3000);
    window.location.reload(false);
  }


  return (
    <>
    <h2 className="mx-5 my-3"> Application demo</h2>
    <div class="mx-5 my-3">
      <Chrono
        items={items}
        mode="VERTICAL_ALTERNATING"
        cardHeight="300px"
        theme={{
          primary: "black",
          secondary: "LightGray",
          cardBgColor: "LightGray",
          cardForeColor: "black",
          titleColor: "black"
        }}
      />
    </div>

    <div class="mx-5 my-3">
      <Accordion defaultActiveKey="0">
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="4">
            <b>0. My profile</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="4">
            <Card.Body>
              <div style={{ textAlign: 'left' }}>
                <Container>
                  <img src={solid} style={{ width:50, height:50 }}></img>
                  <i style={{ textAlign: 'left' }}> <b>My WebID:</b> {session.info.isLoggedIn && session.info.webId} </i>
                </Container>
                <hr></hr>
                <Container>
                  <img src={solid} style={{ width:50, height:50 }}></img>
                  <i style={{ textAlign: 'left' }}> <b>My username:</b> {profile[2]} </i>
                </Container>
                <hr></hr>
                <Container>
                  <img src={did} style={{ width:50, height:50 }}></img>
                  <i style={{ textAlign: 'left' }}> <b>My DID:</b> {profile[1]} </i>
                </Container>
                <hr></hr>
                  <Container>
                    <img src={ethereum} style={{ width:50, height:50 }}></img>
                    <i style={{ textAlign: 'left' }}> <b>My ethereum address:</b> {profile[0]} </i>
                  </Container>
              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="0">
            <b>1. Create credential schema</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <div>
                <Form onSubmit={submitForm} className="mx-5 my-3">
                  <Form.Row className="justify-content-md-center">
                    <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
                      <Form.Label size="lg">Schema name: </Form.Label>
                        <Form.Control
                          type="text"
                          autoComplete="off"
                          placeholder="Enter schema name..."
                          value={name}
                          onChange={(e) => changeName(e.target.value)}
                        />
                    </Form.Group>
                  </Form.Row>
                  <Form.Row className="justify-content-md-center">
                    <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
                      <Form.Label size="lg">Attributes: </Form.Label>
                        <Form.Control
                          type="text"
                          autoComplete="off"
                          placeholder="Attribute1,Attribute2,..."
                          value={attributes}
                          onChange={(e) => changeAttributes(e.target.value)}
                        />
                    </Form.Group>
                  </Form.Row>

                  <Form.Row className="justify-content-md-center">
                    <Form.Group>
                      <Button variant="primary" type="submit">
                        Create schema
                      </Button>
                    </Form.Group>
                  </Form.Row>
                </Form>

                <Alert show={show2} variant="success" className="mx-5 my-3">
                    <Alert.Heading>Schema and credential definition created succesfully.</Alert.Heading>
                    <hr />
                    <div className="d-flex justify-content-end">
                      <Button onClick={() => setShow2(false)} variant="outline-success">
                        Close
                      </Button>
                    </div>
                </Alert>

                <hr></hr>

                <p><b>Created schemas</b></p>

                  <Table size="sm" striped bordered hover variant="dark">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Attributes</th>
                      </tr>
                    </thead>
                    <tbody>
                      {schemas.map((item, i) => {
                        return (
                          <tr key={i}>
                            <td>{item.name}</td>
                            <td>{item.attributes.toString()}</td>
                          </tr>
                        );
                      })
                      }
                    </tbody>
                  </Table>

              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="1">
            <b>2. Create connection offers</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="1">
            <Card.Body>
              <div>
                <Form className="mx-5 my-3">
                  <Form.Row className="justify-content-md-center">
                    <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
                      <Form.Label size="lg">Connection alias: </Form.Label>
                        <Form.Control
                          type="text"
                          autoComplete="off"
                          placeholder="Enter connection alias..."
                          value={alias}
                          onChange={(e) => changeAlias(e.target.value)}
                        />
                    </Form.Group>
                  </Form.Row>
                  <Form.Row className="justify-content-md-center">
                    <Form.Group>
                      <Button variant="outline-primary" onClick={btncreateInvitation}>New invitation</Button>
                    </Form.Group>
                  </Form.Row>
                </Form>

                <Alert show={show} variant="success" className="mx-5 my-3">
                    <Alert.Heading>Please hand the invitation to the other party:</Alert.Heading>
                    <p>
                      {invitation}
                    </p>
                    <hr />
                    <div className="d-flex justify-content-end">
                      <Button onClick={() => setShow(false)} variant="outline-success">
                        Close
                      </Button>
                    </div>
                </Alert>

                <hr></hr>

                  <h5>Connected to the following entities:</h5>

                  <Table size="sm" striped bordered hover variant="dark">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Connection ID</th>
                        <th>Date</th>
                        <th>State</th>
                        <th>Disconnect</th>
                      </tr>
                    </thead>
                    <tbody>
                      {connections.map((item, i) => {
                        return (
                          <tr key={i}>
                            <td>{item.theirLabel}</td>
                            <td>{item.connection_id}</td>
                            <td>{item.created_at}</td>
                            <td>{item.state}</td>
                            <td>
                              <Button variant="light" onClick={() => btndeleteConnection(item.connection_id)}>Delete</Button>
                            </td>
                          </tr>
                        );
                      })
                      }
                    </tbody>
                  </Table>

              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="2">
            <b>3. Issue credentials</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="2">
            <Card.Body>
              <div>
                <p>Credential requests</p>
                  <Table size="sm" striped bordered hover variant="dark">
                    <thead>
                      <tr>
                        <th>Requester name</th>
                        <th>Schema name</th>
                        <th>Requested attributes</th>
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      {requests.map((item, i) => {
                        return (
                          <tr key={i}>
                            <td>{item.name}</td>
                            <td>{item.schema_name}</td>
                            <td><ul>
                              {
                                item.attributes.map((item2, i2) => {
                                  return (
                                    <li key={i2}>Name: '{item2.name}', Value: '{item2.value}'</li>
                                )})
                              }
                            </ul></td>
                            <td><Button variant="light" onClick={() => btnsendCred(item)}>Approve</Button></td>
                            <td><Button variant="light" onClick={() => btndeleteRequest(item)}>Decline</Button></td>
                          </tr>
                        );
                      })
                      }
                    </tbody>
                  </Table>

                  <Alert show={show3} variant="success" className="mx-5 my-3">
                      <Alert.Heading>Credentials have been sent.</Alert.Heading>
                      <div className="d-flex justify-content-end">
                        <Button onClick={() => setShow3(false)} variant="outline-success">
                          Close
                        </Button>
                      </div>
                  </Alert>

                  <Alert show={show4} variant="success" className="mx-5 my-3">
                      <Alert.Heading>Credential request has been deleted.</Alert.Heading>
                      <div className="d-flex justify-content-end">
                        <Button onClick={() => setShow4(false)} variant="outline-success">
                          Close
                        </Button>
                      </div>
                  </Alert>
              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </div>

    </>
  );
};

export default Demo;
