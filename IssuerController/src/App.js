import './App.css';
import React, { useEffect, useMemo, useReducer } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import NavigationBar from "./NavigationBar";
import CreateSchema from "./pages/CreateSchema";
import ManageConnections from "./pages/ManageConnections";
import ManageCredentials from "./pages/ManageCredentials";
import Demo from "./pages/Demo";

function App() {
  return (
    <div className="App">
          <Router>
            <NavigationBar />
            <Switch>
              <Route path="/" exact>
              </Route>
              <Route path="/demo" exact>
                <Demo />
              </Route>
              <Route path="/schema/create" exact>
                <CreateSchema />
              </Route>
              <Route path="/connections" exact>
                <ManageConnections />
              </Route>
              <Route path="/credentials" exact>
                <ManageCredentials />
              </Route>
            </Switch>
          </Router>
    </div>
  );
}

export default App;
