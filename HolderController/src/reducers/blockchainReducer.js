// BLOCKCHAIN
export const BLOCKCHAIN_SETUP = "BLOCKCHAIN_SETUP";
export const STORAGE = "STORAGE";

const blockchainReducer = (state, action) => {
  switch (action.type) {
    case BLOCKCHAIN_SETUP:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export default blockchainReducer;
