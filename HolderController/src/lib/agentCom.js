export const getConnections = async (url) => {

  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const cons = json.results

  const result = new Array()
  for (let index = 0; index < cons.length; index++) {

    let object = cons[index]
    let con = null
    if(object.hasOwnProperty("their_label")){
      let date = new Date(object.created_at)
      let formatted_date = date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes()

      con = new Connection(object.connection_id, formatted_date, object.alias, object.state, object.their_label, object.their_did, object.my_did, object.their_public_did);
    }else{
      let date = new Date(object.created_at)
      let formatted_date = date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes()

      con = new Connection(object.connection_id, formatted_date, object.alias, object.state, null);
    }

    result.push(con)
  }

  return result
};

export const deleteConnection = async (url) => {

  const response = await fetch(url, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;
};

export const createConnection = async (url, data) => {
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data)
  });

  // sleep 3 sec
  await sleep(3000);
  // now a basic message is send to the other agent to promote the connection status to active
  // first get connection id
  if (response.ok) {
      const json = await response.json();
      const connection_id = json.connection_id

      data = {
        "content": "Hello"
      }
      data = JSON.stringify(data)
      const response2 = await fetch("http://localhost:8041/connections/" + connection_id + "/send-message", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: data
      });

  } else {
      alert("HTTP-Error: " + response.status);
  }
};

async function parseAttributes(attributes){
  const att_list = attributes.split(',');
  let result = []
  for (let index = 0; index < att_list.length; index++) {
      const lst = att_list[index].split(':');

      let str = ""
      if(lst.length > 2){
       str = { "name": lst[0] , "value": lst[1] + ":" + lst[2] }
      }else{
        str = { "name": lst[0] , "value": lst[1] }
      }
      result.push(str)
  }

  return result
}

export const requestCredentials = async (url, conID, schemaName, attributes) => {
  // parese attributes
  let att_list = await parseAttributes(attributes)

  const data = {
    "auto_remove": true,
    "comment": "string",
    "connection_id": conID,
    "credential_preview": {
      "@type": "issue-credential/2.0/credential-preview",
      "attributes": att_list
    },
    "filter": {
      "indy": {
        "schema_name": schemaName
      },
      "ld_proof": {
        "credential": {
          "@context": [
            "https://www.w3.org/2018/credentials/v1",
            "https://w3id.org/sampleCredential/v1"
          ],
          "credentialSubject": {
          },
          "issuanceDate": "2019-12-03T12:19:52Z",
          "issuer": "",
          "type": [
            "VerifiableCredential",
            "SampleType"
          ]
        },
        "options": {
          "proofType": "Ed25519Signature2018"
        }
      }
    },
    "trace": true
  };

  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data)
  });
};

export const getCredentials = async (url) => {

  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const creds = json.results

  const result = new Array()
  for (let index = 0; index < creds.length; index++) {
    const cred = new Credential(JSON.stringify(creds[index].attrs), creds[index].schema_id, creds[index].cred_def_id, creds[index].referent);

    result.push(cred)
  }

  return result
};

export const deleteCredential = async (id) => {

  const response = await fetch("http://localhost:8041/credential/" + id, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;
};

export const getPresentationRequests = async (url) => {

  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const reqs = json.results

  const result = new Array()
  for (let index = 0; index < reqs.length; index++) {
    if(reqs[index].state == "request-received"){
      let atts = []
      let attsIds = []
      let obj = reqs[index].by_format.pres_request.indy.requested_attributes
      let cred_def_id = ""
      for (var key in obj){
        cred_def_id = obj[key].restrictions[0].cred_def_id
      }

      for (var att in reqs[index].by_format.pres_request.indy.requested_attributes) {
        atts.push(JSON.stringify(att.split("_")[1]))
        attsIds.push(att)
      }

      let date_created = new Date(reqs[index].created_at)
      let formatted_date = date_created.getDate() + "." + (date_created.getMonth() + 1) + "." + date_created.getFullYear() + " " + date_created.getHours() + ":" + date_created.getMinutes()

      const name = await getNameByConnectionID(reqs[index].connection_id)
      const req = new PresentationRequest(reqs[index].pres_ex_id, reqs[index].connection_id, name, formatted_date, atts, attsIds, cred_def_id);
      result.push(req)
    }
  }

  return result
};

export const getVerifiedPresentations = async (url) => {

  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const reqs = json.results

  const result = new Array()
  for (let index = 0; index < reqs.length; index++) {
    if(reqs[index].state == "done"){

      let att_list = []

      for (var att in reqs[index].by_format.pres_request.indy.requested_attributes) {
        att_list.push(JSON.stringify(att.split("_")[1]))
      }

      let date_created = new Date(reqs[index].created_at)
      let formatted_date = date_created.getDate() + "." + (date_created.getMonth() + 1) + "." + date_created.getFullYear() + " " + date_created.getHours() + ":" + date_created.getMinutes()

      let date_updated = new Date(reqs[index].updated_at)
      let formatted_date2 = date_updated.getDate() + "." + (date_updated.getMonth() + 1) + "." + date_updated.getFullYear() + " " + date_updated.getHours() + ":" + date_updated.getMinutes()

      const name = await getNameByConnectionID(reqs[index].connection_id)
      const req = new VerifiedPresentation(reqs[index].pres_ex_id, reqs[index].connection_id, name, formatted_date, formatted_date2, att_list);
      result.push(req)
    }
  }

  return result
};

export const deletePresentationRequest = async (id) => {

  const response = await fetch("http://localhost:8041/present-proof-2.0/records/" + id, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;
};

export const approvePresentationRequest = async (url, item, cred_id) => {

  const data = {
    "indy": {
      "requested_predicates": {},
      "requested_attributes": {
      },
      "self_attested_attributes": {},
      "trace": false
    },
    "trace": true
  }

  for (let index = 0; index < item.attributeIdentifiers.length; index++) {
    var temp =  {
          "cred_id": cred_id,
          "revealed": true
        }

      var newValue = item.attributeIdentifiers[index]
      data.indy.requested_attributes[newValue] = temp;
  }

  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data)
  });
};

export const getNameByConnectionID = async (conID) => {

  const response = await fetch("http://localhost:8041/connections", {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const reqs = json.results

  let result = ""
  for (let index = 0; index < reqs.length; index++) {
    if(reqs[index].connection_id == conID){
      result = reqs[index].their_label
    }
  }

  return result
};

class Connection {
  constructor(connection_id, created_at, alias, state, theirLabel,  theirDID, myDID, theirpubplicDID) {
    this.connection_id = connection_id;
    this.created_at = created_at;
    this.alias = alias;
    this.state = state;
    this.theirLabel = theirLabel;
    this.theirDID = theirDID;
    this.myDID = myDID;
    this.theirpubplicDID = theirpubplicDID;
  }
}

class Credential {
  constructor(attrs, schema_id, cred_def_id, cred_id){
    this.attrs = attrs;
    this.schema_id = schema_id;
    this.cred_def_id = cred_def_id;
    this.cred_id = cred_id;
  }
}

class PresentationRequest {
  constructor(pres_ex_id, connection_id, name, date, attributes, aIDs, cred_def_id){
    this.pres_ex_id = pres_ex_id
    this.connection_id = connection_id;
    this.name = name;
    this.date = date;
    this.attributes = attributes;
    this.attributeIdentifiers = aIDs;
    this.cred_def_id = cred_def_id;
  }
}

class VerifiedPresentation {
  constructor(pres_ex_id, connection_id, name, createDate, updatedDate, attributes){
    this.pres_ex_id = pres_ex_id
    this.connection_id = connection_id;
    this.name = name;
    this.createDate = createDate;
    this.updatedDate = updatedDate;
    this.attributes = attributes;
  }
}



function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
 }
