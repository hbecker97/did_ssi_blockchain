import './App.css';
import React, { useEffect, useMemo, useReducer } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import NavigationBar from "./NavigationBar";

import { Form, Button, Col, Card, ListGroup, Alert } from "react-bootstrap";

import ManageConnections from "./pages/ManageConnections";
import RequestCredentials from "./pages/RequestCredentials";
import ViewCredentials from "./pages/ViewCredentials";
import ManagePresentationRequests from "./pages/ManagePresentationRequests";
import Overview from "./pages/Overview";
import PurchaseProducts from "./pages/PurchaseProducts";
import Demo from "./pages/Demo";

import getWeb3 from "./lib/getWeb3";
import blockchainReducer from "./reducers/blockchainReducer";
import { BLOCKCHAIN_SETUP } from "./reducers/blockchainReducer";
import BlockchainContext from "./contexts/BlockchainContext";

const App = () => {

  // setup the blockchain state
  const initialBlockchainState = {
    web3: null,
    accounts: null,
  };
  const [blockchainState, dispatch] = useReducer(
    blockchainReducer,
    initialBlockchainState
  );

  // makes sure to only rerender if the state object or the dispatch function changes
  const blockchainContextValue = useMemo(() => {
    return { blockchainState, dispatch };
  }, [blockchainState, dispatch]);

  // setup for web3
  useEffect(() => {
    const setupWeb3 = async () => {
      try {
        // Get network provider and web3 instance.
        const web3 = await getWeb3();

        // Use web3 to get the user's accounts.
        const accounts = await web3.eth.getAccounts();

        // Set web3, accounts, and contract to the application's context
        dispatch({
          type: BLOCKCHAIN_SETUP,
          payload: { web3, accounts },
        });

      } catch (error) {
        // Catch any errors for any of the above operations.
        alert(`Failed to load web3 and accountsCheck console for details.`);
        console.error(error);
      }
    };

    setupWeb3();
  }, []);


  return (
    <div className="App">
       <BlockchainContext.Provider value={blockchainContextValue}>
        <Router>
            <NavigationBar />
            <Switch>
              <Route path="/overview" exact>
                <Overview />
              </Route>
              <Route path="/demo" exact>
                <Demo />
              </Route>
              <Route path="/connections" exact>
                <ManageConnections />
              </Route>
              <Route path="/request" exact>
                <RequestCredentials />
              </Route>
              <Route path="/credentials" exact>
                <ViewCredentials />
              </Route>
              <Route path="/requests" exact>
                <ManagePresentationRequests />
              </Route>
              <Route path="/purchase" exact>
                <PurchaseProducts />
              </Route>
            </Switch>
          </Router>
        </BlockchainContext.Provider>
    </div>
  );
}

export default App;
