import React, { useContext, useState, useEffect } from "react";
import { Form, Button, Col, Card, ListGroup, Alert, InputGroup, FormControl, Table  } from "react-bootstrap";
import { useSession } from "@inrupt/solid-ui-react";

import { fetchProducts, getEthereumAddressFromWebID } from "../lib/solidCom";

import { sendEther, findTransaction} from "../lib/ethereumCom";
import BlockchainContext from "../contexts/BlockchainContext";

const PurchaseProducts = () => {

  const { session } = useSession();

  const [products, changeProducts] = useState([]);
  const [webID, changeWebID] = useState("");
  const [hash, changeHash] = useState("");
  const [show, setShow] = useState(false);
  const { blockchainState } = useContext(BlockchainContext);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try{
      let offersUrl = webID.replace(
         "profile/card#me",
         "offers/offers.ttl"
       );
      //let prod = await getProducts(offersUrl)
      let prod = await fetchProducts(offersUrl, session.fetch)
      changeProducts(prod)
    }catch(error){
      console.log(error)
    }
  }

   async function btnFetchProducts(){
     try{
       const offersUrl = webID.replace(
          "profile/card#me",
          "private/Offers.ttl"
        );
       const prod = await fetchProducts(offersUrl)

     }catch(error){
       console.log(error)
     }
   }

   async function btnbuyProduct(item) {
     const myEthAccount = blockchainState.accounts
     const theirEthAccount = await getEthereumAddressFromWebID(webID, session.fetch)
     let price = parseInt(item.price[1])

     //send ethereum transaction
      const transactionHash = await sendEther(
        blockchainState.web3,
        myEthAccount.toString(),
        theirEthAccount.toString(),
        price.toString(),
      );

      changeHash(transactionHash)
      setShow(true)
   }

   const renderProducts = () => {
       return (
           <tbody>
             {products.map((item, i) => {
             return (
               <tr key={i}>
                <td>
                  <strong>{i + 1}</strong>
                </td>
                <td>
                  {item.name}
                </td>
                <td>
                  {item.price}
                </td>
                <td>
                  {item.currency}
                </td>
                <td>
                  <Button onClick={() => btnbuyProduct(item)}>
                    Buy product
                  </Button>
                </td>
              </tr>
             );
         })}
         </tbody>
       );
     };

  return (
    <>
    <div>
      <h2 className="mx-5 my-3"> Purchase products</h2>
      <Form className="mb-5"  onSubmit={handleSubmit}>
      <Form.Group className="mb-5" controlId="formBasicEmail">
        <Form.Label>WebID</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter webID of verifier..."
          onChange={(e) => changeWebID(e.target.value)}/>
      </Form.Group>
      <Button variant="primary" type="submit">
        Get product list
      </Button>
    </Form>

    <Alert show={show} variant="success" className="mx-5 my-3">
        <Alert.Heading>Succesfully purchased the selected product.</Alert.Heading>
        <hr />
        <p className="text-center">Transaction hash: {hash}</p>
        <div className="d-flex justify-content-end">
          <Button onClick={() => setShow(false)} variant="outline-success">
            Close
          </Button>
        </div>
    </Alert>

    <Table responsive>
      <thead>
        <tr>
          <th>#</th>
          <th>Product name</th>
          <th>Price</th>
          <th>Currency</th>
          <th></th>
        </tr>
      </thead>
    {renderProducts()}
    </Table>
    </div>
    </>
  );
};

export default PurchaseProducts;
