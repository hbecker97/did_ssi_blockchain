import React, { useContext, useState, useEffect } from "react";
import { Form, Button, Col, Card, ListGroup, Alert } from "react-bootstrap";

import { createConnection, getConnections, deleteConnection } from "../lib/agentCom";

const ManageConnections = () => {
  const [invitation, changeInvitation] = useState("");
  const [show, setShow] = useState(false);
  const [connections, changeConnections] = useState([]);

  useEffect(async ()=> {
    const response = await getConnections("http://localhost:8041/connections")
    changeConnections(response)
   }, [] );

  async function btndeleteConnection(id) {

    await deleteConnection("http://localhost:8041/connections/" + id)
    window.location.reload(false);
  }

  function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
   }

  async function btnconnect() {

    setShow(true)
    await createConnection("http://localhost:8041/connections/receive-invitation", invitation)
    await sleep(2000);
    window.location.reload(false);
  }

  const renderConnections = () => {
      return (
        <div>
        {connections.map((item, i) => {
          return (
            <Card className="mx-5 my-3 text-center"  key={i}>
              <Card.Header as="h5">Connection ID: {item.connection_id}</Card.Header>
                <Card.Body>
                  {item.state === "active" && (
                    <Card.Text>
                      <b>Created at: </b> {item.created_at} <br></br>
                      <b>State: </b> {item.state} <br></br>
                      <b>Connected with: </b> {item.theirLabel},  <b>their DID: </b>{item.theirDID} <br></br>
                    </Card.Text>
                  )}
                  {item.state !== "active" && (
                    <Card.Text>
                      <b>Created at: </b> {item.created_at} <br></br>
                      <b>State: </b> {item.state} <br></br>
                    </Card.Text>
                  )}
                  <Button variant="danger" onClick={() => btndeleteConnection(item.connection_id)}>Delete connection</Button>
                </Card.Body>
            </Card>
          );
        })}
        </div>
      );
    };

  return (
    <>
    <h2 className="mx-5 my-3"> Connect to an Issuer</h2>
      <Form className="mx-5 my-3">
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Invitation: </Form.Label>
              <Form.Control
                type="text"
                autoComplete="off"
                placeholder="Enter invitation object..."
                value={invitation}
                onChange={(e) => changeInvitation(e.target.value)}
              />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-md-center">
          <Form.Group>
            <Button variant="outline-primary" onClick={btnconnect}>Connect</Button>
          </Form.Group>
        </Form.Row>
      </Form>

    <Alert show={show} variant="success" className="mx-5 my-3">
        <Alert.Heading>Connection will be set up, please wait...</Alert.Heading>
    </Alert>

    <h2 className="mx-5 my-3"> Manage connections</h2>
    { renderConnections()}
    </>
  );
};

export default ManageConnections;
