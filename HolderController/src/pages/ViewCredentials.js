import React, { useContext, useState, useEffect } from "react";
import { Form, Button, Col, Card, ListGroup, Alert } from "react-bootstrap";

import { getCredentials, deleteCredential } from "../lib/agentCom";

const ViewCredentials = () => {
  const [credentials, changeCredentials] = useState([]);

  useEffect(async ()=> {
    const response = await getCredentials("http://localhost:8041/credentials")
    changeCredentials(response)
   }, [] );

   async function btndeleteCredential(id) {

     await deleteCredential(id)
     window.location.reload(false);
   }

   const renderCredentials = () => {
       return (
         <div>
         {credentials.map((item, i) => {
           return (
             <Card className="mx-5 my-3 text-center"  key={i}>
                 <Card.Body>
                 <Card.Text>
                   <b>Schema ID: </b> {item.schema_id} <br></br>
                   <b>Credential definition ID: </b> {item.cred_def_id} <br></br>
                   <b>My credential id: </b> {item.cred_id}<br></br>
                   <b>Attributes: </b> {item.attrs}<br></br>
                 </Card.Text>
                   <Button variant="danger" onClick={() => btndeleteCredential(item.cred_id)}>Delete credentials</Button>
                 </Card.Body>
             </Card>
           );
         })}
         </div>
       );
     };


  return (
    <>

    {renderCredentials()}

    </>
  );
};

export default ViewCredentials;
