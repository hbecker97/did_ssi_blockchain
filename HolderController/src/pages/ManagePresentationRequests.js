import React, { useContext, useState, useEffect } from "react";
import { Form, Button, Col, Card, ListGroup, Alert, InputGroup, FormControl } from "react-bootstrap";

import { getVerifiedPresentations, getPresentationRequests, deletePresentationRequest, approvePresentationRequest } from "../lib/agentCom";

const ManagePresentationRequests = () => {
  const [show, setShow] = useState(false);
  const [requests, changeRequests] = useState([]);
  const [credID, changeCredID] = useState("");
  const [verifiedPres, changeVerifiedPres] = useState([]);

  useEffect(async ()=> {
    const response = await getPresentationRequests("http://localhost:8041/present-proof-2.0/records")
    changeRequests(response)

    const response2 = await getVerifiedPresentations("http://localhost:8041/present-proof-2.0/records")
    changeVerifiedPres(response2)
   }, [] );

  function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
   }

   async function btnapprove(item) {
     await approvePresentationRequest("http://localhost:8041/present-proof-2.0/records/" + item.pres_ex_id + "/send-presentation", item, credID)
     setShow(true)
     await sleep(2000)
     //window.location.reload(false)
   }

   async function btndelete(id) {

     await deletePresentationRequest(id)
     sleep(1000)
     window.location.reload(false);
   }

  const renderPresentationRequests = () => {
      return (
        <div>
          <Alert show={show} variant="success" className="mx-5 my-3">
              <Alert.Heading>Credential presentation has been sent.</Alert.Heading>
          </Alert>
          {requests.length === 0 && (
            <p>Empty!</p>
          )}
        {requests.map((item, i) => {
          return (
            <Card className="mx-5 my-3 text-center"  key={i}>
              <Card.Header as="h5">Presentation request ID: {item.pres_ex_id}</Card.Header>
                <Card.Body>
                  <Card.Text>
                    <b>Created at: </b> {item.date} <br></br>
                    <b>Created by [connection]: </b> {item.connection_id} <br></br>
                    <b>Created by [name]: </b> {item.name} <br></br>
                    <b>Credential definition ID: </b> {item.cred_def_id} <br></br>
                    <b>Requested attributes: </b> {item.attributes.toString()} <br></br>
                   <br></br>
                     <InputGroup className="mb-3">
                       <InputGroup.Text id="basic-addon1">My credential ID</InputGroup.Text>
                       <FormControl
                         placeholder="Credential ID"
                         value={credID}
                         onChange={(e) => changeCredID(e.target.value)}
                       />
                     </InputGroup>
                  </Card.Text>
                  <Button variant="success" onClick={() => btnapprove(item)}>Approve</Button>&nbsp;
                  <Button variant="danger" onClick={() => btndelete(item.pres_ex_id)}>Decline</Button>
                </Card.Body>
            </Card>
          );
        })}
        </div>
      );
    };

    const renderVerifiedPresentations = () => {
        return (
          <div>
            {verifiedPres.length === 0 && (
              <p>Empty!</p>
            )}
          {verifiedPres.map((item, i) => {
            return (
              <Card className="mx-5 my-3 text-center"  key={i}>
                <Card.Header as="h5">Verified presentation ID: {item.pres_ex_id}</Card.Header>
                  <Card.Body>
                    <Card.Text>
                      <b>Created at: </b> {item.createDate} <br></br>
                      <b>Last updated at: </b> {item.updatedDate} <br></br>
                      <b>Created by [connection ID]: </b> {item.connection_id} <br></br>
                      <b>Created by [name]: </b> {item.name} <br></br>
                      <b>Exposed attributes: </b> {item.attributes.toString()} <br></br>
                     <br></br>
                    </Card.Text>
                  </Card.Body>
              </Card>
            );
          })}
          </div>
        );
      };

  return (
    <>
    <h2 className="mx-5 my-3"> Manage presentation requests</h2>
    { renderPresentationRequests()}

    <h2 className="mx-5 my-3"> View verified presentations</h2>
    { renderVerifiedPresentations()}

    </>
  );
};

export default ManagePresentationRequests;
