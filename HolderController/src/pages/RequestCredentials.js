import React, { useContext, useState, useEffect } from "react";
import { Form, Button, Col, Card, ListGroup, Alert } from "react-bootstrap";

import { requestCredentials } from "../lib/agentCom";

const RequestCredentials = () => {
  const [conID, changeConID] = useState("");
  const [show, setShow] = useState(false);
  const [schemaName, changeSchemaName] = useState("");
  const [attributes, changeAttributes] = useState("");

  useEffect(async ()=> {
    //const response = await getConnections("http://localhost:8041/connections")
    //changeConnections(response)
   }, [] );

   async function submitForm(e) {
     e.preventDefault();

     await requestCredentials("http://localhost:8041/issue-credential-2.0/send-proposal", conID, schemaName, attributes);

     setShow(true)
   }

  return (
    <>
    <h2 className="mx-5 my-3"> Request a credential</h2>

      <Form onSubmit={submitForm} className="mx-5 my-3">
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Connection ID of Issuer: </Form.Label>
              <Form.Control
                type="text"
                autoComplete="off"
                placeholder="Enter connection ID..."
                value={conID}
                onChange={(e) => changeConID(e.target.value)}
              />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Schema name: </Form.Label>
              <Form.Control
                type="text"
                autoComplete="off"
                placeholder="Enter schema name..."
                value={schemaName}
                onChange={(e) => changeSchemaName(e.target.value)}
              />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Attribute list: </Form.Label>
              <Form.Control
                type="text"
                autoComplete="off"
                placeholder="Attribute1:Value1,Attribute2:Value2,..."
                value={attributes}
                onChange={(e) => changeAttributes(e.target.value)}
              />
          </Form.Group>
        </Form.Row>

        <Form.Row className="justify-content-md-center">
          <Form.Group>
            <Button variant="primary" type="submit">
              Request credentials
            </Button>
          </Form.Group>
        </Form.Row>
      </Form>

      <Alert show={show} variant="success" className="mx-5 my-3">
          <Alert.Heading>Credential request has been sent succesfully.</Alert.Heading>
          <hr />
          <div className="d-flex justify-content-end">
            <Button onClick={() => setShow(false)} variant="outline-success">
              Close
            </Button>
          </div>
      </Alert>

    </>
  );
};

export default RequestCredentials;
