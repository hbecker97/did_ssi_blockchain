import React, { useContext, useState, useEffect } from "react";
import { Container, Form, Button, Col, Card, ListGroup, Alert, Accordion, Table } from "react-bootstrap";

import { getProfile, alterQuad } from "../lib/solidCom";
import { getVerifiedPresentations, getPresentationRequests, deletePresentationRequest, approvePresentationRequest, getCredentials, deleteCredential, requestCredentials, createConnection, getConnections, deleteConnection } from "../lib/agentCom";
import { Chrono } from "react-chrono";
import { sendEther, findTransaction} from "../lib/ethereumCom";
import BlockchainContext from "../contexts/BlockchainContext";
import { fetchProducts, getEthereumAddressFromWebID } from "../lib/solidCom";

import { useSession } from "@inrupt/solid-ui-react";

import solid from '../Images/solid.jpeg';
import did from '../Images/did.jpeg';
import ethereum from '../Images/ethereum.png';

const Demo = () => {
  const { session } = useSession();

  // profile & connection
  const [profile, changeProfile] = useState([]);
  const [invitation, changeInvitation] = useState("");
  const [show, setShow] = useState(false);
  const [connections, changeConnections] = useState([]);

  // request credentials
  const [schemaName, changeSchemaName] = useState("");
  const [attributes, changeAttributes] = useState("");
  const [conID, changeConID] = useState("");
  const [show2, setShow2] = useState(false);

  // show credentials
  const [credentials, changeCredentials] = useState([]);

  // presentations
  const [show3, setShow3] = useState(false);
  const [requests, changeRequests] = useState([]);
  const [credID, changeCredID] = useState("");
  const [pres_id, changePresID] = useState("");
  const [verifiedPres, changeVerifiedPres] = useState([]);

  // products
  const [products, changeProducts] = useState([]);
  const [webID, changeWebID] = useState("");
  const [hash, changeHash] = useState("");
  const [show4, setShow4] = useState(false);
  const { blockchainState } = useContext(BlockchainContext);

  const items = [
    {
      title: "1",
      cardTitle: "Create connections",
      //cardSubtitle:"",
      cardDetailedText: "Establish conections between Holder <-> Issuer and Holder <-> Verifier.",
    },
    {
        title: "2",
        cardTitle: "Request credentials",
        //cardSubtitle:"",
        cardDetailedText: "Send a credential request to the Issuer.",
    },
    {
        title: "3",
        cardTitle: "Send credential presentation",
        //cardSubtitle:"",
        cardDetailedText: "Send a credential presentation to the Verifier.",
    },
    {
        title: "4",
        cardTitle: "Purchase products",
        //cardSubtitle:"",
        cardDetailedText: "Purchase products at the store of the Verifier.",
    },
  ];

  useEffect(async ()=> {
    const response = await getConnections("http://localhost:8041/connections")
    changeConnections(response)

    const card = "https://holder.inrupt.net/profile/card#me";
    let pro = await getProfile(card, session.fetch)
    changeProfile(pro)

    const response2 = await getCredentials("http://localhost:8041/credentials")
    changeCredentials(response2)

    const response3 = await getPresentationRequests("http://localhost:8041/present-proof-2.0/records")
    changeRequests(response3)

    const response4 = await getVerifiedPresentations("http://localhost:8041/present-proof-2.0/records")
    changeVerifiedPres(response4)
   }, [] );

  async function btndeleteConnection(id) {

    await deleteConnection("http://localhost:8041/connections/" + id)
    window.location.reload(false);
    //alert(id)
  }

  function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
   }

  async function btnconnect() {

    setShow(true)
    await createConnection("http://localhost:8041/connections/receive-invitation", invitation)
    await sleep(3000);
    window.location.reload(false);
  }

  async function submitForm(e) {
    e.preventDefault();

    await requestCredentials("http://localhost:8041/issue-credential-2.0/send-proposal", conID, schemaName, attributes);

    setShow2(true)
  }

  async function btndeleteCredential(id) {

    await deleteCredential(id)
    window.location.reload(false);
  }

  async function btnapproveRequest(item) {
    await approvePresentationRequest("http://localhost:8041/present-proof-2.0/records/" + item.pres_ex_id + "/send-presentation", item, credID)
    setShow3(true)
    await sleep(3000)
    window.location.reload(false)
  }

  async function btndeleteRequest(id) {

    await deletePresentationRequest(id)
    sleep(1000)
    window.location.reload(false);
  }

  const handleSubmit3 = async (e) => {
    e.preventDefault();
    try{
      let offersUrl = webID.replace(
         "profile/card#me",
         "offers/offers.ttl"
       );
      //let prod = await getProducts(offersUrl)
      let prod = await fetchProducts(offersUrl, session.fetch)
      changeProducts(prod)
    }catch(error){
      console.log(error)
    }
  }

  async function btnFetchProducts(){
    try{
      const offersUrl = webID.replace(
         "profile/card#me",
         "private/Offers.ttl"
       );
      const prod = await fetchProducts(offersUrl)

    }catch(error){
      console.log(error)
    }
  }

  async function btnbuyProduct(item) {
    const myEthAccount = blockchainState.accounts
    const theirEthAccount = await getEthereumAddressFromWebID(webID, session.fetch)
    let price = parseInt(item.price[1])

    //send ethereum transaction
     const transactionHash = await sendEther(
       blockchainState.web3,
       myEthAccount.toString(),
       theirEthAccount.toString(),
       price.toString(),
     );

     changeHash(transactionHash)
     setShow(true)
  }

  const renderProducts = () => {
      return (
          <tbody>
            {products.map((item, i) => {
            return (
              <tr key={i}>
               <td>
                 <strong>{i + 1}</strong>
               </td>
               <td>
                 {item.name}
               </td>
               <td>
                 {item.price}
               </td>
               <td>
                 {item.currency}
               </td>
               <td>
                 <Button onClick={() => btnbuyProduct(item)}>
                   Buy product
                 </Button>
               </td>
             </tr>
            );
        })}
        </tbody>
      );
    };

  return (
    <>
    <h2 className="mx-5 my-3"> Application demo</h2>
    <div class="mx-5 my-3">
      <Chrono
        items={items}
        mode="VERTICAL_ALTERNATING"
        cardHeight="300px"
        theme={{
          primary: "black",
          secondary: "LightGray",
          cardBgColor: "LightGray",
          cardForeColor: "black",
          titleColor: "black"
        }}
      />
    </div>

    <div class="mx-5 my-3">
      <Accordion defaultActiveKey="4">
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="4">
            <b>0. My profile</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="4">
            <Card.Body>
              <div style={{ textAlign: 'left' }}>
                <Container>
                  <img src={solid} style={{ width:50, height:50 }}></img>
                  <i style={{ textAlign: 'left' }}> <b>My WebID:</b> {session.info.isLoggedIn && session.info.webId} </i>
                </Container>
                <hr></hr>
                <Container>
                  <img src={solid} style={{ width:50, height:50 }}></img>
                  <i style={{ textAlign: 'left' }}> <b>My username:</b> {profile[2]} </i>
                </Container>
                <hr></hr>
                <Container>
                  <img src={did} style={{ width:50, height:50 }}></img>
                  <i style={{ textAlign: 'left' }}> <b>My DID:</b> {profile[1]} </i>
                </Container>
                <hr></hr>
                  <Container>
                    <img src={ethereum} style={{ width:50, height:50 }}></img>
                    <i style={{ textAlign: 'left' }}> <b>My ethereum address:</b> {profile[0]} </i>
                  </Container>
              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="0">
            <b>1. Create connections</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <div>
                <Form className="mx-5 my-3">
                  <Form.Row className="justify-content-md-center">
                    <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
                      <Form.Label size="lg">Invitation: </Form.Label>
                        <Form.Control
                          type="text"
                          autoComplete="off"
                          placeholder="Enter invitation object..."
                          value={invitation}
                          onChange={(e) => changeInvitation(e.target.value)}
                        />
                    </Form.Group>
                  </Form.Row>
                  <Form.Row className="justify-content-md-center">
                    <Form.Group>
                      <Button variant="outline-primary" onClick={btnconnect}>Connect</Button>
                    </Form.Group>
                  </Form.Row>
                </Form>

                <Alert show={show} variant="success" className="mx-5 my-3">
                    <Alert.Heading>Connection will be set up, please wait...</Alert.Heading>
                </Alert>

                <hr></hr>
                <h5>Connected to the following entities:</h5>

                <Table size="sm" striped bordered hover variant="dark">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Connection ID</th>
                      <th>Date</th>
                      <th>State</th>
                      <th>Disconnect</th>
                    </tr>
                  </thead>
                  <tbody>
                    {connections.map((item, i) => {
                      return (
                        <tr key={i}>
                          <td>{item.theirLabel}</td>
                          <td>{item.connection_id}</td>
                          <td>{item.created_at}</td>
                          <td>{item.state}</td>
                          <td>
                            <Button variant="light" onClick={() => btndeleteConnection(item.connection_id)}>Delete</Button>
                          </td>
                        </tr>
                      );
                    })
                    }
                  </tbody>
                </Table>
              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="1">
            <b>2. Request credentials</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="1">
            <Card.Body>
              <div>
                <Form onSubmit={submitForm} className="mx-5 my-3">
                  <Form.Row className="justify-content-md-center">
                    <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
                      <Form.Label size="lg">Connection ID of Issuer: </Form.Label>
                        <Form.Control
                          type="text"
                          autoComplete="off"
                          placeholder="Enter connection ID..."
                          value={conID}
                          onChange={(e) => changeConID(e.target.value)}
                        />
                    </Form.Group>
                  </Form.Row>
                  <Form.Row className="justify-content-md-center">
                    <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
                      <Form.Label size="lg">Schema name: </Form.Label>
                        <Form.Control
                          type="text"
                          autoComplete="off"
                          placeholder="Enter schema name..."
                          value={schemaName}
                          onChange={(e) => changeSchemaName(e.target.value)}
                        />
                    </Form.Group>
                  </Form.Row>
                  <Form.Row className="justify-content-md-center">
                    <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
                      <Form.Label size="lg">Attribute list: </Form.Label>
                        <Form.Control
                          type="text"
                          autoComplete="off"
                          placeholder="Attribute1:Value1,Attribute2:Value2,..."
                          value={attributes}
                          onChange={(e) => changeAttributes(e.target.value)}
                        />
                    </Form.Group>
                  </Form.Row>

                  <Form.Row className="justify-content-md-center">
                    <Form.Group>
                      <Button variant="primary" type="submit">
                        Request credentials
                      </Button>
                    </Form.Group>
                  </Form.Row>
                </Form>

                <Alert show={show2} variant="success" className="mx-5 my-3">
                    <Alert.Heading>Credential request has been sent succesfully.</Alert.Heading>
                    <hr />
                    <div className="d-flex justify-content-end">
                      <Button onClick={() => setShow2(false)} variant="outline-success">
                        Close
                      </Button>
                    </div>
                </Alert>

                <hr></hr>

                <p><b>Credential wallet</b></p>

                  <Table size="sm" striped bordered hover variant="dark">
                    <thead>
                      <tr>
                        <th>My credential ID</th>
                        <th>Attributes</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                      {credentials.map((item, i) => {
                        return (
                          <tr key={i}>
                            <td>{item.cred_id}</td>
                            <td>{item.attrs}</td>
                            <td>
                              <Button variant="light" onClick={() => btndeleteCredential(item.cred_id)}>Delete</Button>
                            </td>
                          </tr>
                        );
                      })
                      }
                    </tbody>
                  </Table>

              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="2">
            <b>3. Send credential presentation</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="2">
            <Card.Body>
              <div>
                <p>Presentation requests</p>
                  <Alert show={show} variant="success" className="mx-5 my-3">
                      <Alert.Heading>Credential presentation has been sent.</Alert.Heading>
                  </Alert>
                  <Table size="sm" striped bordered hover variant="dark">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Created by</th>
                        <th>Requested attributes</th>
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      {requests.map((item, i) => {
                        return (
                          <tr key={i}>
                            <td>{item.pres_ex_id}</td>
                            <td>{item.name}</td>
                            <td>{item.attributes.toString()}</td>
                            <td><Button variant="light" onClick={() => btnapproveRequest(item)}>Approve</Button></td>
                            <td><Button variant="light" onClick={() => btndeleteRequest(item.pres_ex_id)}>Decline</Button></td>
                          </tr>
                        );
                      })
                      }
                    </tbody>
                  </Table>

                  <Form className="mx-5 my-3">
                    <Form.Row className="justify-content-md-center">
                      <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
                        <Form.Label size="lg">My credential ID: </Form.Label>
                          <Form.Control
                            type="text"
                            autoComplete="off"
                            placeholder="Enter credential ID..."
                            value={credID}
                            onChange={(e) => changeCredID(e.target.value)}
                          />
                      </Form.Group>
                    </Form.Row>
                  </Form>

                <hr></hr>

                <p>Verified requests</p>
                  <Table size="sm" striped bordered hover variant="dark">
                    <thead>
                      <tr>
                        <th>Created by</th>
                        <th>Exposed attributes</th>
                      </tr>
                    </thead>
                    <tbody>
                      {verifiedPres.map((item, i) => {
                        return (
                          <tr key={i}>
                            <td>{item.name}</td>
                            <td>{item.attributes.toString()}</td>
                          </tr>
                        );
                      })
                      }
                    </tbody>
                  </Table>

              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="3">
            <b>4. Purchase products</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="3">
            <Card.Body>
              <div>
                <Form className="mb-5"  onSubmit={handleSubmit3}>
                  <Form.Group className="mb-5" controlId="formBasicEmail">
                    <Form.Label>WebID</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter webID of verifier..."
                      onChange={(e) => changeWebID(e.target.value)}/>
                  </Form.Group>
                  <Button variant="primary" type="submit">
                    Get product list
                  </Button>
                </Form>

                <Alert show={show} variant="success" className="mx-5 my-3">
                    <Alert.Heading>Succesfully purchased the selected product.</Alert.Heading>
                    <hr />
                    <p className="text-center">Transaction hash: {hash}</p>
                    <div className="d-flex justify-content-end">
                      <Button onClick={() => setShow(false)} variant="outline-success">
                        Close
                      </Button>
                    </div>
                </Alert>

                <Table responsive>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Product name</th>
                      <th>Price</th>
                      <th>Currency</th>
                      <th></th>
                    </tr>
                  </thead>
                {renderProducts()}
                </Table>
              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </div>

    </>
  );
};

export default Demo;
