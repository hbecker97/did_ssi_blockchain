export const createInvitation = async (url) => {
  const data = {}
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data)
  });
  return response.json()
};

export const getConnections = async (url) => {

  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const cons = json.results

  const result = new Array()
  for (let index = 0; index < cons.length; index++) {

    let object = cons[index]
    let con = null
    if(object.hasOwnProperty("their_label")){
      let date = new Date(object.created_at)
      let formatted_date = date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes()

      con = new Connection(object.connection_id, formatted_date, object.alias, object.state, object.their_label, object.their_did, object.my_did, object.their_public_did);
    }else{
      let date = new Date(object.created_at)
      let formatted_date = date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes()

      con = new Connection(object.connection_id, formatted_date, object.alias, object.state, null);
    }

    result.push(con)
  }

  return result
};

export const deleteConnection = async (url) => {

  const response = await fetch(url, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;
};


async function getPublicDID(){
  const response = await fetch("http://localhost:8051/wallet/did/public", {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  let _did = await response.json()

  return _did.result.did
}

export const sendPresentationRequest = async (url, conID, cred_def_id, attributes) => {

  var data = {
    "comment": "DID + SoliD",
    "connection_id": conID,
    "presentation_request": {
      "indy": {
        "name": "Proof of concept",
        "version": "1.0",
        "requested_attributes": {
        },
        "requested_predicates": {
        }
      }
    }
  }

  let att_list = attributes.split(",")

  for (let index = 0; index < att_list.length; index++) {
    var temp =  {
          "name": att_list[index],
          "restrictions": [
            {
              "cred_def_id": cred_def_id
            }
          ]
        }

      var newValue = index + "_" + att_list[index];
      data.presentation_request.indy.requested_attributes[newValue] = temp;
  }

  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data)
  });
  if (response.status !== 200) return null;
};

export const getPresentations = async (url) => {

  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const reqs = json.results

  const result = new Array()
  for (let index = 0; index < reqs.length; index++) {
    if(reqs[index].state == "presentation-received"){

      let att_list = []

      for (var att in reqs[index].by_format.pres_request.indy.requested_attributes) {
        att_list.push(JSON.stringify(att.split("_")[1]))
      }

      let att_list2 = []
      let values = []

      let obj = reqs[index].by_format.pres.indy.requested_proof.revealed_attrs
      for (var key in obj){
        values.push(obj[key].raw)
      }

      for (var att in reqs[index].by_format.pres.indy.requested_proof.revealed_attrs) {
        att_list2.push(JSON.stringify(att.split("_")[1]))
      }

      const name = await getNameByConnectionID(reqs[index].connection_id)
      const req = new ReceivedPresentation(reqs[index].pres_ex_id, reqs[index].connection_id, name, reqs[index].updated_at, att_list, att_list2, values);
      result.push(req)
    }
  }

  return result
};

export const declinePresentation = async (url) => {

  const response = await fetch(url, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;
};

export const verifyPresentation = async (url) => {

  const data = {}
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data)
  });
};

export const getVerifiedPresentations = async (url) => {

  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const reqs = json.results

  const result = new Array()
  for (let index = 0; index < reqs.length; index++) {
    if(reqs[index].verified == "true"){

      let att_list = []

      for (var att in reqs[index].by_format.pres_request.indy.requested_attributes) {
        att_list.push(JSON.stringify(att.split("_")[1]))
      }

      let att_list2 = []
      let values = []

      let obj = reqs[index].by_format.pres.indy.requested_proof.revealed_attrs
      for (var key in obj){
        values.push(obj[key].raw)
      }

      for (var att in reqs[index].by_format.pres.indy.requested_proof.revealed_attrs) {
        att_list2.push(JSON.stringify(att.split("_")[1]))
      }

      //let date = parseDate(reqs[index].updated_at)
      let date = new Date(reqs[index].updated_at)
      let formatted_date = date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes()

      const name = await getNameByConnectionID(reqs[index].connection_id)
      const req = new ReceivedPresentation(reqs[index].pres_ex_id, reqs[index].connection_id, name, formatted_date, att_list, att_list2, values);

      result.push(req)
    }
  }

  return result
};

export const getNameByConnectionID = async (conID) => {

  const response = await fetch("http://localhost:8051/connections", {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const reqs = json.results

  let result = ""
  for (let index = 0; index < reqs.length; index++) {
    if(reqs[index].connection_id == conID){
      result = reqs[index].their_label
    }
  }

  return result
};

class Connection {
  constructor(connection_id, created_at, alias, state, theirLabel, theirDID, myDID, theirpubplicDID) {
    this.connection_id = connection_id;
    this.created_at = created_at;
    this.alias = alias;
    this.state = state;
    this.theirLabel = theirLabel
    this.theirDID = theirDID;
    this.myDID = myDID;
    this.theirpubplicDID = theirpubplicDID;
  }
}

class ReceivedPresentation {
  constructor(pres_ex_id, connection_id, name, updatedDate, reqAttributes, revAttributes, values){
    this.pres_ex_id = pres_ex_id
    this.connection_id = connection_id;
    this.name = name
    this.updatedDate = updatedDate;
    this.reqAttributes = reqAttributes;
    this.revAttributes = revAttributes;
    this.values = values;
  }
}
