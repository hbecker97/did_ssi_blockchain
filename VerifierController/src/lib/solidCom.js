import { Writer, Parser, Store, DataFactory } from "n3";

export const parseToN3 = async (text) => {
  const store = new Store();
  const parser = new Parser();

  return new Promise((resolve, reject) => {
    parser.parse(text, (error, quad) => {
      if (error) reject(error);
      if (quad) store.addQuad(quad);
      else resolve(store);
    });
  });
};

export const requestRDF = async (url, fetch) => {
  const response = await fetch(url, {
    headers: {
      Accept: "text/turtle",
    },
  });

  if (response.status !== 200) return null;

  // parse rdf triples
  const rawData = await response.text();
  const store = await parseToN3(rawData);
  return store;
};

export const requestHead = async (url, fetch) => {
  const response = await fetch(url, {
    method: "HEAD",
  });

  return response;
};

export const checkAvailability = async (url, fetch) => {
  const response = await requestHead(url, fetch);

  switch (response.status) {
    case 200:
      return "Authorized";
    case 401:
    case 403:
      return "Unauthorized";
    case 404:
      return "N/A";
    default:
      return "Error";
  }
};

export const getProfile = async (url, fetch) => {

  const response = await requestRDF(url, fetch)

  //if (response.status !== 200) return null;

  // parse RDF data
  // get ethereum address
  let ethAddr = response.getObjects(
    null,
    "http://ethon.consensys.net/address"
  )[0].id

  // get DID
  let did = ""
  let quads = response.getQuads()
  for(let i=0; i < quads.length; i++){
    if(quads[i].predicate.id === '#did')
    did = quads[i].object.id
  }

  // get name
  let name = ""
  let quads2 = response.getQuads()
  for(let i=0; i < quads2.length; i++){
    if(quads2[i].predicate.id === 'http://xmlns.com/foaf/0.1/name')
    name = quads2[i].object.id
  }

  return [ethAddr, did, name]
};

export const alterQuad = async (url) => {

  const response = await requestRDF("https://verifier.inrupt.net/profile/card#me")

  //if (response.status !== 200) return null;

  // get all quads
  const store = new Store();

  let quads = response.getQuads()
  for(let i=0; i < quads.length; i++){
    if(quads[i].predicate.id === '#did')
      return null
  }

  //return [ethAddr, did]
};

export const fetchOrders = async (ordersUrl, fetch) => {
  let orders = new Array()

  if(await checkAvailability(ordersUrl, fetch) === "Unauthorized"){
    alert("You are not authorized to access the ressource!")
    return null;
  }else{
    const response = await requestRDF(ordersUrl, fetch)
    // get all quads
    let subjects = response.getSubjects()

    for(let i=0; i < subjects.length; i++){
      // retrieve information about every subject
      let name = response.getObjects(subjects[i], "https://schema.org/orderedItem")
      let status = response.getObjects(subjects[i], "https://schema.org/orderStatus")
      let date = response.getObjects(subjects[i], "https://schema.org/orderDate")
      let customer = response.getObjects(subjects[i], "https://schema.org/customer")

      orders.push(new Order(name[0]["id"], status[0]["id"], date[0]["id"], customer[0]["id"]))
    }
    return orders
  }
}

class Order {
  constructor(name, state, date, customer){
    this.name = name
    this.state = state;
    this.date = date;
    this.customer = customer;
  }
}
