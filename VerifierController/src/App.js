import './App.css';
import React, { useEffect, useMemo, useReducer } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import NavigationBar from "./NavigationBar";
import ManageConnections from "./pages/ManageConnections";
import RequestPresentation from "./pages/RequestPresentation";
import VerifyPresentation from "./pages/VerifyPresentations";
import Overview from "./pages/Overview";
import OrderManagement from "./pages/OrderManagement";
import Demo from "./pages/Demo";

function App() {
  return (
    <div className="App">
          <Router>
            <NavigationBar />
            <Switch>
              <Route path="/overview" exact>
                <Overview />
              </Route>
              <Route path="/demo" exact>
                <Demo />
              </Route>
              <Route path="/connections" exact>
                <ManageConnections />
              </Route>
              <Route path="/requests" exact>
                <RequestPresentation />
              </Route>
              <Route path="/verify" exact>
                <VerifyPresentation />
              </Route>
              <Route path="/orders" exact>
                <OrderManagement />
              </Route>
            </Switch>
          </Router>
    </div>
  );
}

export default App;
