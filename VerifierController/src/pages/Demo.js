import React, { useContext, useState, useEffect } from "react";
import { Container, Form, Button, Col, Card, ListGroup, Alert, Accordion, Table } from "react-bootstrap";

import { getProfile, alterQuad } from "../lib/solidCom";
import { getPresentations, declinePresentation, verifyPresentation, getVerifiedPresentations, sendPresentationRequest, createInvitation, getConnections, deleteConnection } from "../lib/agentCom";
import { Chrono } from "react-chrono";

import { useSession } from "@inrupt/solid-ui-react";

import solid from '../Images/solid.jpeg';
import did from '../Images/did.jpeg';
import ethereum from '../Images/ethereum.png';

import { fetchOrders } from "../lib/solidCom";

const Demo = () => {
  const { session } = useSession();

  // profile & connection
  const [profile, changeProfile] = useState([]);
  const [invitation, changeInvitation] = useState("");
  const [show, setShow] = useState(false);
  const [connections, changeConnections] = useState([]);
  const [alias, changeAlias] = useState("");

  //request presentation
  const [conID, changeConID] = useState("");
  const [credDefId, changeCredDefId] = useState("");
  const [attributes, changeAttributes] = useState("");
  const [show2, setShow2] = useState(false);

  // verify presentation
  const [recPres, changeRecPres] = useState([]);
  const [verPres, changeVerPres] = useState([]);
  const [show3, setShow3] = useState(false);

  // orders
  const [orders, changeOrders] = useState([]);
  const [webID, changeWebID] = useState("");

  const items = [
    {
        title: "1",
        cardTitle: "Create connection offers",
        //cardSubtitle:"",
        cardDetailedText: "Hand the connection offers to Holder to establish a connection.",
    },
    {
        title: "2",
        cardTitle: "Request credential presentation",
        //cardSubtitle:"",
        cardDetailedText: "Send a credential presentation request to the Holder.",
    },
    {
        title: "3",
        cardTitle: "Verify credential presentation",
        //cardSubtitle:"",
        cardDetailedText: "Verify received credential presentations.",
    },
    {
        title: "4",
        cardTitle: "Manage products and orders",
        //cardSubtitle:"",
        cardDetailedText: "Manage available products and received orders.",
    },
  ];

  useEffect(async ()=> {
    const response = await getConnections("http://localhost:8051/connections")
    changeConnections(response)

    const card = "https://verifier.inrupt.net/profile/card#me";
    let pro = await getProfile(card, session.fetch)
    changeProfile(pro)

    const response3 = await getPresentations("http://localhost:8051/present-proof-2.0/records")
    changeRecPres(response3)

    const response2 = await getVerifiedPresentations("http://localhost:8051/present-proof-2.0/records")
    changeVerPres(response2)

    try{
      let ordersUrl = session.info.webId.replace(
         "profile/card#me",
         "orders/orders.ttl"
       );
      let ord = await fetchOrders(ordersUrl, session.fetch)
      changeOrders(ord)
    }catch(error){
      console.log(error)
    }
   }, [] );

   async function btncreateInvitation(e) {
     const response = await createInvitation("http://localhost:8051/connections/create-invitation?alias=" + alias)

     let inv = ""
     inv = response.invitation
     inv = JSON.stringify(inv)

     changeInvitation(inv)
     setShow(true)
   }

  async function btndeleteConnection(id) {

    await deleteConnection("http://localhost:8051/connections/" + id)
    window.location.reload(false);
  }

  function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
   }

  async function btnconnect() {

    setShow(true)
    //await createConnection("http://localhost:8051/connections/receive-invitation", invitation)
    await sleep(2000);
    //window.location.reload(false);
  }

  async function btnsendRequest(e) {
    const response = await sendPresentationRequest("http://localhost:8051/present-proof-2.0/send-request", conID, credDefId, attributes)

    setShow2(true)
  }

  async function btnverify(id) {
    const response = await verifyPresentation("http://localhost:8051/present-proof-2.0/records/" + id + "/verify-presentation")
    setShow3(true)
  }

  async function btndecline(id) {
    const response = await declinePresentation("http://localhost:8051/present-proof-2.0/records/" + id)
    sleep(1000)
    window.location.reload(false);
  }

  return (
    <>
    <h2 className="mx-5 my-3"> Application demo</h2>
    <div class="mx-5 my-3">
      <Chrono
        items={items}
        mode="VERTICAL_ALTERNATING"
        cardHeight="300px"
        theme={{
          primary: "black",
          secondary: "LightGray",
          cardBgColor: "LightGray",
          cardForeColor: "black",
          titleColor: "black"
        }}
      />
    </div>

    <div class="mx-5 my-3">
      <Accordion defaultActiveKey="0">
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="4">
            <b>0. My profile</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="4">
            <Card.Body>
              <div style={{ textAlign: 'left' }}>
                <Container>
                  <img src={solid} style={{ width:50, height:50 }}></img>
                  <i style={{ textAlign: 'left' }}> <b>My WebID:</b> {session.info.isLoggedIn && session.info.webId} </i>
                </Container>
                <hr></hr>
                <Container>
                  <img src={solid} style={{ width:50, height:50 }}></img>
                  <i style={{ textAlign: 'left' }}> <b>My username:</b> {profile[2]} </i>
                </Container>
                <hr></hr>
                <Container>
                  <img src={did} style={{ width:50, height:50 }}></img>
                  <i style={{ textAlign: 'left' }}> <b>My DID:</b> {profile[1]} </i>
                </Container>
                <hr></hr>
                  <Container>
                    <img src={ethereum} style={{ width:50, height:50 }}></img>
                    <i style={{ textAlign: 'left' }}> <b>My ethereum address:</b> {profile[0]} </i>
                  </Container>
              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="0">
            <b>1. Create connection offers</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <div>
                <Form className="mx-5 my-3">
                  <Form.Row className="justify-content-md-center">
                    <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
                      <Form.Label size="lg">Connection alias: </Form.Label>
                        <Form.Control
                          type="text"
                          autoComplete="off"
                          placeholder="Enter connection alias..."
                          value={alias}
                          onChange={(e) => changeAlias(e.target.value)}
                        />
                    </Form.Group>
                  </Form.Row>
                  <Form.Row className="justify-content-md-center">
                    <Form.Group>
                      <Button variant="outline-primary" onClick={btncreateInvitation}>New invitation</Button>
                    </Form.Group>
                  </Form.Row>
                </Form>

                <Alert show={show} variant="success" className="mx-5 my-3">
                    <Alert.Heading>Please hand the invitation to the other party:</Alert.Heading>
                    <p>
                      {invitation}
                    </p>
                    <hr />
                    <div className="d-flex justify-content-end">
                      <Button onClick={() => setShow(false)} variant="outline-success">
                        Close
                      </Button>
                    </div>
                </Alert>

                <hr></hr>

                  <h5>Connected to the following entities:</h5>

                  <Table size="sm" striped bordered hover variant="dark">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Connection ID</th>
                        <th>Date</th>
                        <th>State</th>
                        <th>Disconnect</th>
                      </tr>
                    </thead>
                    <tbody>
                      {connections.map((item, i) => {
                        return (
                          <tr key={i}>
                            <td>{item.theirLabel}</td>
                            <td>{item.connection_id}</td>
                            <td>{item.created_at}</td>
                            <td>{item.state}</td>
                            <td>
                              <Button variant="light" onClick={() => btndeleteConnection(item.connection_id)}>Delete</Button>
                            </td>
                          </tr>
                        );
                      })
                      }
                    </tbody>
                  </Table>
              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="1">
            <b>2. Request credential presentation</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="1">
            <Card.Body>
              <div>
                <Form className="mx-5 my-3">
                  <Form.Row className="justify-content-md-center">
                    <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
                      <Form.Label size="lg">Connection ID: </Form.Label>
                        <Form.Control
                          type="text"
                          autoComplete="off"
                          placeholder="Enter connection id..."
                          value={conID}
                          onChange={(e) => changeConID(e.target.value)}
                        />
                    </Form.Group>
                  </Form.Row>
                  <Form.Row className="justify-content-md-center">
                    <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
                      <Form.Label size="lg">Credential definition ID: </Form.Label>
                        <Form.Control
                          type="text"
                          autoComplete="off"
                          placeholder="Enter credential definition ID..."
                          value={credDefId}
                          onChange={(e) => changeCredDefId(e.target.value)}
                        />
                    </Form.Group>
                  </Form.Row>
                  <Form.Row className="justify-content-md-center">
                    <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
                      <Form.Label size="lg">Required attributes: </Form.Label>
                        <Form.Control
                          type="text"
                          autoComplete="off"
                          placeholder="Attribute1,Attribute2..."
                          value={attributes}
                          onChange={(e) => changeAttributes(e.target.value)}
                        />
                    </Form.Group>
                  </Form.Row>
                  <Form.Row className="justify-content-md-center">
                    <Form.Group>
                      <Button variant="outline-primary" onClick={btnsendRequest}>Send request</Button>
                    </Form.Group>
                  </Form.Row>
                </Form>

              <Alert show={show2} variant="success" className="mx-5 my-3">
                  <Alert.Heading>Presentation request has been sent to credential holder.</Alert.Heading>
                  <div className="d-flex justify-content-end">
                    <Button onClick={() => setShow2(false)} variant="outline-success">
                      Close
                    </Button>
                  </div>
              </Alert>
              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="2">
            <b>3. Verify credential presentation</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="2">
            <Card.Body>
              <div>
                <p>Received presentations</p>
                  <Table size="sm" striped bordered hover variant="dark">
                    <thead>
                      <tr>
                        <th>Holder name</th>
                        <th>Requested attributes</th>
                        <th>Reveiled values</th>
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      {recPres.map((item, i) => {
                        return (
                          <tr key={i}>
                            <td>{item.name}</td>
                            <td>{item.reqAttributes.toString()}</td>
                            <td>{item.values.toString()}</td>
                            <td><Button variant="light" onClick={() => btnverify(item.pres_ex_id)}>Verify</Button></td>
                            <td><Button variant="light" onClick={() => btndecline(item.pres_ex_id)}>Decline</Button></td>
                          </tr>
                        );
                      })
                      }
                    </tbody>
                  </Table>

                  <Alert show={show3} variant="success" className="mx-5 my-3">
                      <Alert.Heading>Presentation has been verified.</Alert.Heading>
                  </Alert>

                <hr></hr>

                <p>Verified presentations</p>
                  <Table size="sm" striped bordered hover variant="dark">
                    <thead>
                      <tr>
                        <th>Holder name</th>
                        <th>Requested attributes</th>
                        <th>Reveiled values</th>
                      </tr>
                    </thead>
                    <tbody>
                      {verPres.map((item, i) => {
                        return (
                          <tr key={i}>
                            <td>{item.name}</td>
                            <td>{item.reqAttributes.toString()}</td>
                            <td>{item.values.toString()}</td>
                          </tr>
                        );
                      })
                      }
                    </tbody>
                  </Table>

              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="3">
            <b>4. Manage products and orders</b>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="3">
            <Card.Body>
              <div>
                <Table size="sm" striped bordered hover variant="dark">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Product name</th>
                      <th>Customer</th>
                      <th>State</th>
                      <th>Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    {orders.map((item, i) => {
                    return (
                      <tr key={i}>
                       <td>
                         <strong>{i + 1}</strong>
                       </td>
                       <td>
                         {item.name}
                       </td>
                       <td>
                         {item.customer}
                       </td>
                       <td>
                         {item.state}
                       </td>
                       <td>
                         {item.date}
                       </td>
                     </tr>
                    );
                })}
                  </tbody>
                </Table>
              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </div>

    </>
  );
};

export default Demo;
