import React, { useContext, useState, useEffect } from "react";
import { Form, Button, Col, Card, ListGroup, Alert } from "react-bootstrap";

import { sendPresentationRequest } from "../lib/agentCom";

const RequestPresentation = () => {
  const [conID, changeConID] = useState("");
  const [credDefId, changeCredDefId] = useState("");
  const [attributes, changeAttributes] = useState("");
  const [show, setShow] = useState(false);

  useEffect(async ()=> {
   }, [] );

  async function btnsendRequest(e) {
    const response = await sendPresentationRequest("http://localhost:8051/present-proof-2.0/send-request", conID, credDefId, attributes)

    setShow(true)
  }


  return (
    <>
    <h2 className="mx-5 my-3"> Send holder a presentation request</h2>
      <Form className="mx-5 my-3">
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Connection ID: </Form.Label>
              <Form.Control
                type="text"
                autoComplete="off"
                placeholder="Enter connection id..."
                value={conID}
                onChange={(e) => changeConID(e.target.value)}
              />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Credential definition ID: </Form.Label>
              <Form.Control
                type="text"
                autoComplete="off"
                placeholder="Enter credential definition ID..."
                value={credDefId}
                onChange={(e) => changeCredDefId(e.target.value)}
              />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Required attributes: </Form.Label>
              <Form.Control
                type="text"
                autoComplete="off"
                placeholder="Attribute1,Attribute2..."
                value={attributes}
                onChange={(e) => changeAttributes(e.target.value)}
              />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-md-center">
          <Form.Group>
            <Button variant="outline-primary" onClick={btnsendRequest}>Send request</Button>
          </Form.Group>
        </Form.Row>
      </Form>

    <Alert show={show} variant="success" className="mx-5 my-3">
        <Alert.Heading>Presentation request has been sent to credential holder.</Alert.Heading>
        <div className="d-flex justify-content-end">
          <Button onClick={() => setShow(false)} variant="outline-success">
            Close
          </Button>
        </div>
    </Alert>
    </>
  );
};

export default RequestPresentation;
