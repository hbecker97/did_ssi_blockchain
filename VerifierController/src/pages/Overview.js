import React, { useContext, useState, useEffect } from "react";
import { Form, Button, Col, Card, ListGroup, Alert, InputGroup, FormControl } from "react-bootstrap";
import { useSession } from "@inrupt/solid-ui-react";

import { getProfile, alterQuad } from "../lib/solidCom";


const Overview = () => {
  const { session } = useSession();

  const [profile, changeProfile] = useState([]);
  const [ethAddr, changeEthAddr] = useState([]);

  useEffect(async ()=> {
      const card = "https://verifier.inrupt.net/profile/card#me";
      let profile = await getProfile(card, session.fetch)
      changeProfile(profile)
   }, [] );

   async function btnChangeAddress() {
     //await alterQuad()
   }

  return (
    <>
    <div>
      <h2 className="mx-5 my-3"> Solid information</h2>
      <p> <b>My WebID:</b> {session.info.isLoggedIn && session.info.webId} </p>
      <p> <b>My username:</b> {session.info.isLoggedIn && session.info.webId.split("/")[2].split(".")[0]} </p>
      <p> <b>My DID:</b> {profile[1]} </p>
      <p> <b>My ethereum address:</b> {profile[0]} </p>
        <InputGroup className="mb-3">
          <Button variant="outline-secondary" id="button-addon1" onClick={() => btnChangeAddress()}>
            Change Address
          </Button>
          <FormControl
            placeholder="Ethereum address"
            value={ethAddr}
            onChange={(e) => changeEthAddr(e.target.value)}
          />
        </InputGroup>
    </div>
    </>
  );
};

export default Overview;
