import React, { useContext, useState, useEffect } from "react";
import { Form, Button, Col, Card, ListGroup, Alert, InputGroup, FormControl, Table  } from "react-bootstrap";
import { useSession } from "@inrupt/solid-ui-react";

import { fetchOrders } from "../lib/solidCom";

const OrderManagement = () => {
  const { session } = useSession();

  const [orders, changeOrders] = useState([]);
  const [webID, changeWebID] = useState("");

  useEffect(async ()=> {
    try{
      let ordersUrl = session.info.webId.replace(
         "profile/card#me",
         "orders/orders.ttl"
       );
      let ord = await fetchOrders(ordersUrl, session.fetch)
      changeOrders(ord)
    }catch(error){
      console.log(error)
    }
   }, [] );

   const renderOrders = () => {
       return (
           <tbody>
             {orders.map((item, i) => {
             return (
               <tr key={i}>
                <td>
                  <strong>{i + 1}</strong>
                </td>
                <td>
                  {item.name}
                </td>
                <td>
                  {item.customer}
                </td>
                <td>
                  {item.state}
                </td>
                <td>
                  {item.date}
                </td>
              </tr>
             );
         })}
         </tbody>
       );
     };

  return (
    <>
    <div>
      <Table responsive>
        <thead>
          <tr>
            <th>#</th>
            <th>Product name</th>
            <th>Customer</th>
            <th>State</th>
            <th>Date</th>
          </tr>
        </thead>
      {renderOrders()}
      </Table>
    </div>
    </>
  );
};

export default OrderManagement;
