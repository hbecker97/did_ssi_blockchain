import React, { useContext, useState, useEffect } from "react";
import { Form, Button, Col, Card, ListGroup, Alert } from "react-bootstrap";

import { getPresentations, declinePresentation, verifyPresentation, getVerifiedPresentations } from "../lib/agentCom";

const VerifyPresentation = () => {
  const [recPres, changeRecPres] = useState([]);
  const [verPres, changeVerPres] = useState([]);
  const [show, setShow] = useState(false);

  useEffect(async ()=> {
    const response = await getPresentations("http://localhost:8051/present-proof-2.0/records")
    changeRecPres(response)

    const response2 = await getVerifiedPresentations("http://localhost:8051/present-proof-2.0/records")
    changeVerPres(response2)
   }, [] );

  async function btnverify(id) {
    const response = await verifyPresentation("http://localhost:8051/present-proof-2.0/records/" + id + "/verify-presentation")
    setShow(true)
  }

  async function btndecline(id) {
    const response = await declinePresentation("http://localhost:8051/present-proof-2.0/records/" + id)
    sleep(1000)
    window.location.reload(false);
  }

  function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
   }

  const renderPresentations = () => {
      return (
        <div>
          <Alert show={show} variant="success" className="mx-5 my-3">
              <Alert.Heading>Presentation has been verified.</Alert.Heading>
          </Alert>

          {recPres.length === 0 && (
            <p>Empty!</p>
          )}

          {recPres.map((item, i) => {
            return (
              <Card className="mx-5 my-3 text-center"  key={i}>
                <Card.Header as="h5">Presentation request ID: {item.pres_ex_id}</Card.Header>
                  <Card.Body>
                    <Card.Text>
                      <b>Updated at: </b> {item.updatedDate} <br></br>
                      <b>Created by [connection]: </b> {item.connection_id} <br></br>
                      <b>Created by [name]: </b> {item.name} <br></br>
                      <b>Requested attributes: </b> {item.reqAttributes.toString()} <br></br>
                      <b>Reveiled attributes: </b> {item.revAttributes.toString()} <br></br>
                      <b>Reveiled values: </b> {item.values.toString()} <br></br>
                     <br></br>
                    </Card.Text>
                    <Button variant="success" onClick={() => btnverify(item.pres_ex_id)}>Verify</Button>&nbsp;
                    <Button variant="danger" onClick={() => btndecline(item.pres_ex_id)}>Decline</Button>
                  </Card.Body>
              </Card>
            );
          })}

        </div>
      );
    };

    const renderVerifiedPresentations = () => {
        return (
          <div>
            {verPres.length === 0 && (
              <p>Empty!</p>
            )}

            {verPres.map((item, i) => {
              return (
                <Card className="mx-5 my-3 text-center"  key={i}>
                  <Card.Header as="h5">Presentation request ID: {item.pres_ex_id}</Card.Header>
                    <Card.Body>
                      <Card.Text>
                        <b>Updated at: </b> {item.updatedDate} <br></br>
                        <b>Holder agent [connection]: </b> {item.connection_id} <br></br>
                        <b>Holder agent [name]: </b> {item.name} <br></br>
                        <b>Requested attributes: </b> {item.reqAttributes.toString()} <br></br>
                        <b>Reveiled attributes: </b> {item.revAttributes.toString()} <br></br>
                        <b>Reveiled values: </b> {item.values.toString()} <br></br>
                       <br></br>
                      </Card.Text>
                    </Card.Body>
                </Card>
              );
            })}
          </div>
        );
      };

  return (
    <>
    <h2 className="mx-5 my-3"> Verify received presentations</h2>

    { renderPresentations()}

    <h2 className="mx-5 my-3"> Verified presentations</h2>

    { renderVerifiedPresentations()}
    </>
  );
};

export default VerifyPresentation;
