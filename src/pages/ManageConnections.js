import React, { useContext, useState, useEffect } from "react";
import { Form, Button, Col, Card, ListGroup, Alert } from "react-bootstrap";

import { createInvitation, getConnections } from "../lib/agentCom";

const ManageConnections = () => {
  const [invitation, changeInvitation] = useState("");
  const [show, setShow] = useState(false);
  const [connections, changeConnections] = useState([]);

  useEffect(async ()=> {
    const response = await getConnections("http://localhost:8031/connections")
    changeConnections(response)
    //console.log(response)
   }, [] );

  async function btncreateInvitation(e) {
    const response = await createInvitation("http://localhost:8031/connections/create-invitation")

    let inv = ""
    inv = response.invitation
    inv = JSON.stringify(inv)

    changeInvitation(inv)
    setShow(true)
  }

  async function btndeleteConnection(id) {

    //alert(id)
    console.log(id)
  }

  const renderConnections = () => {
      return (
        <div>
        {connections.map((item, i) => {
          return (
            <Card className="mx-5 my-3 text-center"  key={i}>
              <Card.Header as="h5">Connection ID: {item.connection_id}</Card.Header>
              <Card.Body>
                <Card.Text>
                  <p><b>Created at: </b> {item.created_at} </p>
                  <p><b>Alias: </b> {item.alias} </p>
                  <p><b>State: </b> {item.state} </p>
                </Card.Text>
                <Button variant="danger" onClick={item.connection_id) => btndeleteConnection}>Delete connection</Button>
              </Card.Body>
            </Card>
          );
        })}
        </div>
      );
    };

  return (
    <>
    <h2 className="mx-5 my-3"> Create invitation</h2>
    <Button variant="outline-primary" onClick={btncreateInvitation}>New invitation</Button>

    <Alert show={show} variant="success" className="mx-5 my-3">
        <Alert.Heading>Please hand the invitation to the other party:</Alert.Heading>
        <p>
          {invitation}
        </p>
        <hr />
        <div className="d-flex justify-content-end">
          <Button onClick={() => setShow(false)} variant="outline-success">
            Close
          </Button>
        </div>
    </Alert>

    <h2 className="mx-5 my-3"> Manage connections</h2>
    { renderConnections()}
    </>
  );
};

export default ManageConnections;
