import React, { useContext, useState, useEffect } from "react";
import { Form, Button, Col, Card, ListGroup } from "react-bootstrap";

import { getSchema, createSchema } from "../lib/agentCom";

const CreateSchema = () => {
  const [name, changeName] = useState("");
  const [attributes, changeAttributes] = useState("");
  const [schemas, changeSchemas] = useState([]);

  useEffect(async ()=> {
    const respond = await getSchema("http://localhost:8031/schemas/created", "http://localhost:8031/schemas/")
    changeSchemas(respond)
   }, [] );

  async function submitForm(e) {
    e.preventDefault();
    const att_list = attributes.split(',');
    let list = "["
    for (let index = 0; index < att_list.length; index++) {
        list += '"' + att_list[index] + '",'
    }
    list = list.substring(0, list.length - 1);
    list += "]"

    await createSchema("http://localhost:8031/schemas", name, att_list);
  }

  const renderSchemas = () => {
      return (
        <div>
          {schemas.map((item, i) => {
            return (
              <Card className="mx-5 my-3 text-center"  key={i}>
                <Card.Header as="h5"><b>Name: </b>{item.name}</Card.Header>
                <Card.Body>
                  <Card.Title><b>Attributes: </b></Card.Title>
                  <ListGroup variant="flush">
                    <ListGroup.Item>{item.attributes.toString()}</ListGroup.Item>
                  </ListGroup>
                </Card.Body>
              </Card>
            );
          })}
        </div>
      );
    };


  return (
    <>
    <h2 className="mx-5 my-3">Create a credential schema</h2>
      <Form onSubmit={submitForm} className="mx-5 my-3">
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Schema name: </Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter schema name..."
                value={name}
                onChange={(e) => changeName(e.target.value)}
              />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-md-center">
          <Form.Group controlId="formResourceBrowse" className="mx-5 my-3">
            <Form.Label size="lg">Attributes: </Form.Label>
              <Form.Control
                type="text"
                placeholder="Attribute1,Attribute2,..."
                value={attributes}
                onChange={(e) => changeAttributes(e.target.value)}
              />
          </Form.Group>
        </Form.Row>

        <Form.Row className="justify-content-md-center">
          <Form.Group>
            <Button variant="primary" type="submit">
              Create
            </Button>
          </Form.Group>
        </Form.Row>
      </Form>

    <h2 className="mx-5 my-12">View created schema</h2>

    {renderSchemas()}
    </>
  );
};

export default CreateSchema;
