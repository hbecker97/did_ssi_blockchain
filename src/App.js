import './App.css';
import React, { useEffect, useMemo, useReducer } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import NavigationBar from "./NavigationBar";
import CreateSchema from "./pages/CreateSchema";
import ManageConnections from "./pages/ManageConnections";

function App() {
  return (
    <div className="App">
          <Router>
            <NavigationBar />
            <Switch>
              <Route path="/" exact>
              </Route>
              <Route path="/schema/create" exact>
                <CreateSchema />
              </Route>
              <Route path="/connections" exact>
                <ManageConnections />
              </Route>
            </Switch>
          </Router>
    </div>
  );
}

export default App;
