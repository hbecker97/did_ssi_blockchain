export const sendMessage = async (url, data) => {
  await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },

    body: {
            "content": data
    },
  });
};

export const createInvitation = async (url) => {
  const data = {}
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data)
  });
  return response.json()
};

export const getConnections = async (url) => {
  // first get call to get schema ids
  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const cons = json.results

  // second get call to get schema_ids
  const result = new Array()
  for (let index = 0; index < cons.length; index++) {

    let object = cons[index]
    let con = new Connection(object.connection_id, object.created_at, object.alias, object.state);
    result.push(con)
  }

  return result
};

export const createSchema = async (url, name, attributes) => {
  const data = {
      "attributes": attributes,
      "schema_name": name,
      "schema_version": "1.0"
    };
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json"
    },
    body: JSON.stringify(data)
  });
  //console.log(response)
};

export const getSchema = async (url, url2) => {
  // first get call to get schema ids
  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
    },
  });

  if (response.status !== 200) return null;

  // parse json object
  const json = await response.json();
  const schema_ids = json.schema_ids


  // second get call to get schema_ids
  const result = new Array()
  for (let index = 0; index < schema_ids.length; index++) {
    const url_con =  url2 + schema_ids[index]

    const response = await fetch(url_con, {
      headers: {
        Accept: "application/json",
      },
    });

    if (response.status !== 200) return null;

    // parse json object
    const json = await response.json();

    let schema = new Schema(json.schema.name, json.schema.attrNames);
    result.push(schema)
  }

  return result

};

class Schema {
  constructor(name, attList) {
    this.name = name;
    this.attributes = attList;
  }
}

class Connection {
  constructor(connection_id, created_at, alias, state) {
    this.connection_id = connection_id;
    this.created_at = created_at;
    this.alias = alias;
    this.state = state;
  }
}
