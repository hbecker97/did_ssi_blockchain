## Hyperledger Aries + Solid PODs

This application combines verifiable credential exchange and their usage for authorized transactions using Hyperledger Aries (Aries Cloud-Agent Python) and Solid PODs. The picture below shows the architecture of the system:

![drawing](./Pictures/AppAufbau.PNG)

Three parties will use the application: credential issuer "Ida", credential holder "Heinrich" and credential Verifier "Viktor".
Every participant uses his/her Solid POD to store identity information in the profile (see below) and uses a controller application that communicates with a unique agent. The controller applications allow the users to perform several activities in the context of verifiable credentials.
The agents are able to communicate with each other and also with the Hyperleder infrastructure (which is the von-network). "Heinrich" and "Viktor" are able to send/receive money that is send using Ethereum transactions. Therefore, their controller applications are able to communicate with an Ethereum blockchain. Lastly, business transactions are modeled through 'products' that are saved in "Viktor's" Solid POD. "Heinrich" will be able to buy these products, but only after he has identified himself with credentials, that "Ida" has issued and "Viktor" has verified. The Solid access mechanism is used to grant access to "Viktor's" products for verified users. Therefore "Ida" will issue credentials to "Heinrich" that will
contain the WebID of "Heinrich". The process that will be showcased later, is summarized by the following diagram:

![drawing](./Pictures/SolidVC.png)


### Set-up

Several steps must be performed in order to use the application. In the following, the steps needed to run the application locally are described.
It is strongly recommended to run everything on a Linux distribution! Further, you should use Google Chrome as browser.

0. <ins>Prerequisites</ins>
  - Install docker on your machine
  - Install docker compose on your machine
  - Install git on your machine
  - Install python and pip on your machine
  - Install npm on your machine

  Since aries-cloudagents are used for the communication, the needed packages are:
  - aries-cloudagent version 0.7.0 (https://pypi.org/project/aries-cloudagent/0.7.0/)
  - python3_indy (pip install python3_indy)
  - libindy, please refer [here](https://ldej.nl/post/becoming-a-hyperledger-aries-developer-part-2-development-environment/) for further information

  Please refer [here](https://github.com/hyperledger/aries-cloudagent-python/blob/main/DevReadMe.md) for further information on aries-cloudagents.

  Further, [Ganache](https://www.trufflesuite.com/ganache) needs to be set up.
  Next, the [MetaMask](https://metamask.io/) browser extension needs to be installed and connected to you Ganache blockchain.

  Now you are ready to go with the application provided in this repository!

1. <ins>Set-up VON Network</ins>

  The VON Network will serve as distributed ledger. You can find further information [here](https://github.com/bcgov/von-network/blob/master/docs/UsingVONNetwork.md).

  Begin by getting to a `bash` command line and clone the `von-network` repo:

  ```
  git clone https://github.com/bcgov/von-network
  cd von-network

  ```

  Once the repo has been cloned, we'll build docker images for the VON Network and then start your Indy network:

  ```
  ./manage build
  ./manage start --logs

  ```

2. <ins>Register agents on the network</ins>

  Please head to http://localhost:9000. You need to register the Issuer and Verifier agents that will be used in the following.
   - IssuerAgent000000000000000000001
   - VerifierAgent0000000000000000001

  Leave the other fields empty and press `Register DID`.

  ![drawing](./Pictures/agent_register.PNG)

  You will receive the DIDs of the two agents as output. These will be used later.

3. <ins>Starting the agents.</ins>

  You can start the aries-cloudagents from any directory on your device, after you have installed them globally before.
  Open a bash terminal for every agent and start the agents locally:

  - Issuer agent:
      ```
      aca-py start --label IssuerAgent --inbound-transport http 0.0.0.0 8030 --outbound-transport http --admin 0.0.0.0 8031 --admin-insecure-mode --endpoint http://localhost:8030/ --genesis-url http://localhost:9000/genesis --seed IssuerAgent000000000000000000001 --debug-connections --auto-provision --wallet-type indy --wallet-name IssuerAgent --wallet-key secret --public-invites --auto-accept-invites --auto-accept-requests --auto-store-credential
      ```

  - Verifier agent:
      ```
      aca-py start --label VerifierAgent --inbound-transport http 0.0.0.0 8050 --outbound-transport http --admin 0.0.0.0 8051 --admin-insecure-mode --endpoint http://localhost:8050/ --genesis-url http://localhost:9000/genesis --seed VerifierAgent0000000000000000001 --debug-connections --auto-provision --wallet-type indy --wallet-name VerifierAgent --wallet-key secret --public-invites --auto-accept-invites --auto-accept-requests --auto-store-credential
      ```

  - Holder agent:
      ```
      aca-py start --label HolderAgent --inbound-transport http 0.0.0.0 8040 --outbound-transport http --admin 0.0.0.0 8041 --admin-insecure-mode --endpoint http://localhost:8040/ --genesis-url http://localhost:9000/genesis --debug-connections --auto-provision --wallet-type indy --wallet-name HolderAgent --wallet-key secret --auto-accept-invites --auto-accept-requests --auto-store-credential --auto-respond-credential-proposal --auto-respond-credential-offer
      ```

  The behavior of the agents and their communication will be logged in the terminals in the following. You should see the following in the terminal:

  ![drawing](./Pictures/AgentCMD.PNG)

  If you need more information about the agent set-up process, you can type
    ```
    aca-py start --help
    ```
  to get information about the available properties.

4. <ins>Starting the controller applications</ins>

  Issuer, Verifier and Holder need a controller application to perform their activities. Therefore, three controller applications need to be started in the following. Open three additional terminals and navigate into the three directories: HolderController, IssuerController and VerifierController. In all these terminals do the following:

        npm install
        npm run

  Three controller applications are running now. Head to your browser (chrome is recommended) and navigate to the following pages:
    - http://localhost:3002/ for the Issuer controller
    - http://localhost:3001/ for the Verifier controller
    - http://localhost:3000/ for the Holder controller

Great! The agents and their controller applications are set up. Further, you will have to create three Solid PODs with Inrupt as the provider.
The Solid profiles are used to combine WebID, DID and Ethereum account of every participant. The information marked is needed to be inserted in the according Solid profile:

  ![drawing](./Pictures/SolidProfile.png)

Of course, the used DID that was created when the agent was registered on the distributed ledger needs to be used. As Ethon address, you can use one of the sample accounts that are available at Ganache blockchain.


### Workflow

In the following, the worklflow is showcased that is performed to issue verifiable credentials and execute verified business transactions.
You can perform the showcased actions by using the "Demo" page in the controller applications. Important: After you have performed an action, please reload your browser window to load the new state.

1. <ins>Workflow inspection</ins>

  First of all, every controller application visualizes the workflow of the specific user:

  ![drawing](./Pictures/HolderWorkflow.PNG)

  A drop-down menu is used below and follows these steps. That will make it easy to perform the steps in the correct order.


2. <ins>Profile information</ins>

  After the Solid PODs are set up and the users are logged in (use the login button in the upper right corner), the Solid profile with the included IDs are displayed under step 0 in the controller application:

  ![drawing](./Pictures/HolderProfile.PNG)


3. <ins>Establish connections</ins>

  In the first step, two connections need to be be established: Holder <-> Issuer ans Holder <-> Verifier.
  Therefore, Verifier and Issuer have the ability to create an connection offer:

  ![drawing](./Pictures/CreateConnectionOffer.PNG)

  You can enter any alias. In the next step, the invitation object in the braces must be inserted into the invitation field in the Holder application:

  ![drawing](./Pictures/CreateConnection.PNG)

  You can see the active connections in the table of every controller application.


4. <ins>Create a credential schema</ins>

  Next, the Issuer needs to create a credential schema. Therefore a schema name and a comma separated list of attributes needs to be chosen. For our use case, the WebID needs to be part of the credentials. You can use any further attributes:

  ![drawing](./Pictures/createSchema.PNG)

  The credential schema is now registered on the distributed ledger for everyone to see.


5. <ins>Request credentials</ins>

  In the next step, the Holder will request credentials of the schema that the issuer has created. Therefore, you can send a request from the Holder application to the Issuer. Insert the connection ID of the Issuer (to be found in the table under connections) and the credential schema name. Lastly, a list of the attributes and their values that are requested can be inserted (please follow the given format!):

  ![drawing](./Pictures/RequestCredentials.PNG)

  The WebID needs to be the WebID of the Holder of course.


6. <ins>Issue credentials</ins>

  Back at the issuer application, you can now see a request from the Holder.
  Click 'Approve' to issue credentials with the given values:

  ![drawing](./Pictures/IssueCredentials.PNG)

  Going back to the Holder application, you can now see the credentials in the credential wallet:

  ![drawing](./Pictures/credentialWallet.PNG)


7. <ins>Request credential presentation</ins>

  Now that the Holder has verifiable credentials stored in his wallet, the Verifier can request a presentation of them using the application:

  ![drawing](./Pictures/RequestCredentialPresentation.PNG)

  Therefore, the connection ID that connects with the holder must be inserted and a list of the requested attributes (the WebID in this use case). Further, the credential definition ID is needed. It can be found at http://localhost:9000/browse/domain and type = "CRED_DEF" (the needed ID is marked in orange):

  ![drawing](./Pictures/CredDefinitionLedger.PNG)

  After pressing 'request', the request is send to the Holder.


8. <ins>Send presentation</ins>

  Back at the Holder application, the Holder now has received the request. The request can now be approved or declined. To approve it, the according credential ID needs to be inserted (to be in the credential wallet above):

  ![drawing](./Pictures/SendPresentation.PNG)


9. <ins>Verify presentation</ins>

  The Verifier has now received a credential presentation with the needed attributes. You can verify the presentation:

  ![drawing](./Pictures/VerifyPresentation.PNG)

  That's it. The Holder is no verified by the Verifier and the Verifier can be sure, that he has received the correct WebID of the Holder, because it was part of the verifiable credentials.
  Further, the WebID of the Holder can now be used by the Verifier to grant access to the products of the Verifier. This functionality is not yet supported, but it is showcased in the following.

  First of all, the Verifier has two files in his Solid POD: a offer list and a order list:

  ![drawing](./Pictures/Orders.PNG)

  ![drawing](./Pictures/Offers.PNG)

  After the Verifier has verified the credentials of the Holder, he allows the Holder to access the order list by using the Solid POD access mechanism (ACL):

  ![drawing](./Pictures/ACL.PNG)

  The holder can now view the product list in the controller application:

  ![drawing](./Pictures/purchaseProducts.PNG)

  Therefore, the WebID of the Verifier needs to be inserted. The Holder can buy a product by clicking the button. That will execute an Ethereum transaction, that needs to approved in the meta mask window:

  ![drawing](./Pictures/SendEther.PNG)

  The requested amount of Ether will be transferred to the Ethereum wallet of the Verifier. Further, the order shall be added to the Verifier's order file that is shown above. The Verifier can now read the order list in the controller application:

  ![drawing](./Pictures/ManageProducts.PNG)

  Again, this functionality is not yet implemented.

This application showcases, how the Hyperleder framework and Solid PODs can be combined to execute verified business transactions. Further, WebIDs, DIDs and Ethereum accounts are combined using Solid profiles and RDF.
